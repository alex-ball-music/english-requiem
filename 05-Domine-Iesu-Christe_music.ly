\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key f \minor
  \time 6/4
  \tempo "Urgently" 4=120
  \partial 4
}

tmControl = {
  s4
  s1.*12 \bar"||"\mark\default
  s1.*16 \bar"||"\mark\default
  s1.*16 \bar"||"\mark\default \tempo "With exhilaration"
  s1.*16 \bar"||"\mark\default
  s1.*12
  \override TextSpanner.bound-details.left.text = "rall."
  s2.\startTextSpan s2.
  s1.
  s2. s2\stopTextSpan \bar"|."
}

tempotrack = {
  s4
  s1.*72
  \tempo 4=116
  s4
  \tempo 4=114
  s4
  \tempo 4=112
  s4
  \tempo 4=110
  s4
  \tempo 4=108
  s4
  \tempo 4=106
  s4
  \tempo 4=104
  s4
  \tempo 4=100
  s4
  \tempo 4=98
  s4
  \tempo 4=96
  s4
  \tempo 4=94
  s4
  \tempo 4=92
  s4
  \tempo 4=90
  s4
  \tempo 4=88
  s2
}

saControl = {
  s4
  s1.*4

  \set Staff.shortInstrumentName = \markup \center-column { "S." "A." }
  s1.\f
  s1.*3

  \set Staff.shortInstrumentName = #""
  s1.*3
  s2. s2

  s4\mp
  s1.*8

  s1.\f
  s1.*6
  s2. s2

  s4\mf
  s1.
  s2.\< s2.\>
  s1.\!
  s1.*5

  s1.\f
  s1.*7

  s1.*8
  s1.\ff
  s1.*7

  s1.\mf
  s2. s2.\<
  s1.\f
  s1.*5

  s1.\ff
}

tbControl = {
  s4
  s1.*4

  \set Staff.shortInstrumentName = \markup \center-column { "T." "B." }
  s1.\f
  s1.*3

  \set Staff.shortInstrumentName = #""
  s1.*4

  s1.*3
  s2. s4 s2\mf
  s1.*2
  s1.\<
  s2. s4 s2\!

  s1.\f
  s1.*5

  s1.*4
  s2. s2 s4\mp\<
  s1.
  s2.\f s2.\>
  s1.
  s2.\p\< s2.
  s1.\!

  s1.\f
  s1.*7

  s1.*8
  s1.\ff
  s1.*7

  s1.\mf
  s2. s2.\<
  s1.\f
  s1.*5

  s1.\ff
}

pnControl = {
  s4\f
  s1.*11
  s2. s2.\>

  s1.\mp
  s1.*2
  s1.\<
  s1.\mf
  s1.
  s1.\<
  s2. s2 s4\f

  s1.*7
  s2. s2.\>

  s1.\mf
  s2.\< s2.\>
  s1.\mp
  s1.\<
  s1.\!
  s1.\>
  s1.\p\<
  s2. s2 s4\f
  s1.*8

  s1.*7
  s2. s2 s4\ff

  s1.*7
  s2. s2.\>

  s1.\mf
  s2. s2.\<
  s1.\f
  s1.*4
  s2. s2 s4\ff
}


soprano = \relative c'' {
  \global
  % Music follows here.
  r4
  R1.*4

  af4 4 4 g2 r4
  r4 af bf c f,4 r
  af4 4 4 af2 r4
  r4 af bf c f,4 r

  R1.*3
  r2.^\markup{ \halign #1 { \smcp { sopranos } and \smcp { altos } } } r2

  c4
  f4 f c' bf2 g4
  af2 af4 g f ef
  f4 c2 ~ 4 r2
  R1.*5

  af'4 4 4 g2 r4
  r4 af bf c f,4 r
  af4 4 4 af2 r4
  r4 af bf c f,4 r

  R1.*3
  r2. r2

  c4
  f4 c'4 c4 ~ 4 bf af
  g( af bf c bf) af
  g2. r2.
  R1.*5

  af4 4 4 g2 r4
  r4 af bf c f,4 r
  af4 4 4 af2 r4
  r4 af bf c f,4 r

  R1.*3
  r2. r4

  af4 bf
  c2 af4 bf2 c4
  af4 bf c ~ c bf2
  c2. ~ 2 r4
  r2. r2 c4
  ef4 ef c df2 c4
  bf4 af af ~ 4 af bf
  c df ef df c bf
  af2. ~ 2 r4

  af4 4 4 g2 r4
  r4 af bf c f,4 r
  af4 4 4 af2 r4
  r4 af bf c f,4 r

  R1.*4

  af4 af af g2 bf4
  c2 af4 f c' df
  ef4 df c df c bf
  af2. r2 df4
  ef2 c4 df2 c4
  bf4 af2 ~ 4 af bf
  c4 af2 ~ 2.
  R1.

  af4 4 4 g2 r4
  r4 af bf c f,4 r
  af4 4 4 af2 r4
  r4 af bf c2( df4)
  ef1. ~ 1.

  r2. r2
}

alto = \relative c' {
  \global
  % Music follows here.
  r4
  R1.*4

  f4 4 4 f2 r4
  r4 f f f f r
  f4 4 4 g2 r4
  r4 f f f f r

  R1.*3
  r2. r2

  c4
  f4 f c' bf2 g4
  af2 af4 g f ef
  f4 c2 ~ 4 r2
  R1.*5

  f4 4 4 f2 r4
  r4 f f f f r
  f4 4 4 g2 r4
  r4 f f f f r

  R1.*3
  r2. r2

  c4
  f4 c'4 c4 ~ 4 bf af
  g( f ef df ef) f
  g2. r2.
  R1.*5

  f4 4 4 f2 r4
  r4 f f f f r
  f4 4 4 g2 r4
  r4 f f f f r

  R1.*3
  r2. r4

  af4 bf
  af2 ef4 g2 g4
  f4 g af ~ 4 f2
  af2.( bf2) r4
  r2. r2 af4
  c4 c af bf2 af4
  f4 f4 f( ff4) 4 gf
  af bf c bf af gf
  f2.( ff2) r4

  ef4 4 4 ef2 r4
  r4 f f f f r
  ef4 4 4 f2 r4
  r4 f f f f r
  R1.*4

  af4 af af g2 bf4
  c2 af4 f af bf
  c4 bf af bf af g
  gf2. r2 af4
  c2 af4 bf2 af4
  f4 f2( ff4) ff ff
  ef4 ef2( df2.)
  R1.

  ef4 4 4 ef2 r4
  r4 f f f f r
  ef4 4 4 f2 r4
  r4 f f f2( af4)
  af1. ~ 1.

  r2. r2
}

tenor = \relative c' {
  \global
  % Music follows here.
  r4
  R1.*4

  c4 c c df2 r4
  r4 c c df df r
  c4 c c df2 r4
  r4 c c df df r

  R1.*4

  R1.*3
  r2. r4 c4 c
  f2.( ef2)  df4
  c4 c c d d bf
  c1. ~ 2. ~ 4 r2

  c4 c c df2 r4
  r4 c c df df r
  c4 c c df2 r4
  r4 c c df df r

  R1.*4

  R1.*2
  r2. r2 c,4
  f g af g af bf
  c2. c4 bf af
  bf( af g f) ef df
  c2. c2.
  R1.

  c'4 c c df2 r4
  r4 c c df df r
  c4 c c df2 r4
  r4 c c df df r

  R1.*3
  r2. r4

  c4 bf
  af2 c4 bf2 ef4
  f4 f4 f2 df2
  c2.( bf2) r4
  r2. r2 df4
  ef4 ef c df2 c4
  bf4 af af ~ 4 af bf
  c df ef df c bf
  af2. ~ 2 r4

  c4 c c bf2 r4
  r4 c c df df r
  c4 c c bf2 r4
  r4 c c df df r
  R1.*4

  af4 af af g2 bf4
  c2 af4 f c' df
  ef4 df c df c bf
  df2. r2 df4
  ef2 c4 df2 c4
  bf4 af2 ~ 4 af bf
  c4 af2 ~ 2.
  R1.

  c4 c c bf2 r4
  r4 c c df df r
  c4 c c bf2 r4
  r4 c c df2.
  df2.( ef c1.)

  r2. r2
}

bass = \relative c {
  \global
  % Music follows here.
  r4
  R1.*4

  f4 4 4 f2 r4
  r4 f f df df r
  f4 4 4 f2 r4
  r4 f f df df r

  R1.*4

  R1.*3
  r2. r4 c4 c
  f2.( ef2)  df4
  c4 c c d d bf
  af1.( g2. ~ 4) r r

  f'4 4 4 f2 r4
  r4 f f df df r
  f4 4 4 f2 r4
  r4 f f df df r

  R1.*4

  R1.*2
  r2. r2 c4
  f g af g f df
  c2. c'4 bf af
  g( f ef df) c bf
  c2. c2.
  R1.

  f4 4 4 f2 r4
  r4 f f df df r
  f4 4 4 f2 r4
  r4 f f df df r

  R1.*3
  r2. r4

  df4 f4
  af2 af4 ef2 ef4
  f4 f ef( df) df2
  af'2.( ef2) r4
  r2. r2 af4
  af4 af af gf2 gf4
  df4 4 4 ~ 4 4 bf'4
  af4 af af gf4 gf gf
  df2. ~ df2 r4

  af'4 4 4 ef2 r4
  r4 f ef df df r
  af'4 4 4 ef2 r4
  r4 f ef df df r
  R1.*4

  af'4 af af g2 bf4
  c2 af4 f af bf
  af4 4 4 ef4 ef f
  gf2. r2 f4
  af2 af4 gf2 gf4
  f4 df2 ~ 4 df4 ef
  af,4 af2 ~ af2.
  R1.

  af'4 4 4 ef2 r4
  r4 f ef df df r
  af'4 4 4 ef2 r4
  r4 f ef df2.
  af1. ~ 1.

  r2. r2
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  \repeat unfold 1 { \sh }
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  \repeat unfold 16 { \sh }

  De -- li -- ver the souls of all the faith -- ful de -- par -- ted.

  \repeat unfold 16 { \sh }

  De -- li -- ver them from the li -- on’s mouth.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Lord Je -- sus Christ, King of Glo -- ry,
  Lord Je -- sus Christ, King of Glo -- ry.

  From the pains of hell
  and the bot -- tom -- less pit. __

  Lord Je -- sus Christ, King of Glo -- ry,
  Lord Je -- sus Christ, King of Glo -- ry.

  So hell may not swal -- low them up,
  nor may they fall in -- to dark -- ness.

  Lord Je -- sus Christ, King of Glo -- ry,
  Lord Je -- sus Christ, King of Glo -- ry.

  Sac -- ri -- fice and prayer we of -- fer you, O Lord:
  ac -- cept them for those de -- par -- ted souls
  of whom we make me -- mo -- rial this day.

  Lord Je -- sus Christ, King of Glo -- ry,
  Lord Je -- sus Christ, King of Glo -- ry.

  Grant them, O Lord, to pass from death
  to the life which you pro -- mised of old
  to A -- bra -- ham and Sa -- rah and their child -- ren.

  Lord Je -- sus Christ, King of Glo -- ry.
  Lord Je -- sus Christ, King of Glo -- ry.
  Lord Je -- sus Christ, King of Glo -- ry.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  \repeat unfold 1 { \sh }
}

right = \relative c'' {
  \global
  r4
  r4 <af f c> q <g f df>2.
  r4 <af f c> <bf f c> <c f, df> << { \voiceOne f,2 } \new Voice { \voiceTwo df4 bf } >> \oneVoice
  c4 <af' f c> q <g f df> <af f df> <bf f df>
  <c f, d> <bf f d> <af f d> <f df>2 r4

  <af f c>4 q q <g f df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> bf,
  <af' f c>4 q q <af g df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> r

  f4 <c' f af> q <df f g>2.
  f,4 <af c f>4 q <f c' ef>2 <f bf df>4
  c'4 <af f c> q <g f df> <af f df> <bf f df>
  <c f, d>4 <bf f d> <af f d> <f df>2 c4

  <af' f c>2 q4 <g f df>2 q4
  <af f c>2 q4 <g f df>2 q4
  <af f c>2 q4 <g f df>2 q4
  <af f c>2 q4 <g f c>2 q4
  <f c af>2 q4 <ef c af>2 <df c af>4
  <c af f>2 q4 <d bf f>2 q4
  <ef c af>2. <ef c bf af>2.
  <ef c g>2. <e bf g>4 q c4

  <af' f c>4 q q <g f df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> bf,
  <af' f c>4 q q <af g df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> r

  f4 <c' f af> q <df f g>2.
  f,4 <af c f>4 q <f c' ef>2 <f bf df>4
  c'4 <af f c> q <g f df> <af f df> <bf f df>
  <c f, d>4 <bf f d> <af f d> <f df>2 c4

  <af' f c>2 q4 <g f df>2 q4
  <ef g>4 <f af> <g bf> <af c> <g bf> <f af>
  <g ef c>2. <g f df>2.
  <af f d>2. <g f df>2.
  <af f c>2 q4 <g f df>2 q4
  <g ef df>2. <f df bf>2 <f df bf>4
  <g f c>2. <g d c>2.
  <g ef c>2. <g e bf>4 q c,

  <af' f c>4 q q <g f df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> bf,
  <af' f c>4 q q <af g df>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> r

  f4 <c' f af> q <df f g>2.
  f,4 <af c f>4 q <f c' ef>2 <f bf df>4
  c'4 <af f c> q <g f df> <af f df> <bf f df>
  <c f, d>4 <bf f d> <af f d> <f df>2 f4

  <ef af c>2 q4 <ef g bf>2 q4
  <f af>2 <f af c>4 ~ 4 <f af bf>2
  <ef af c>2 q4 <f g bf>2 q4
  <df gf bf>2 q4 <df f af>2 q4
  <ef af c>2 q4 <gf bf df>2 <gf bf c>4
  <df f af>2 q4 <df ff af>2 <df gf bf>4
  <ef af c>2 q4 <gf bf df>2 <gf bf>4
  <df f af>2 q4 <df ff af>2 df4

  <af' ef c>4 q q <g f bf,>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> df
  <af' ef c>4 q q << { \voiceOne af2 g4 } \new Voice { \voiceTwo <f bf,>2. } >> \oneVoice
  r4 <af f c> <bf f c> <c f, df> <f, df> g
  af4 <c ef af> q << { \voiceOne <g' bf,>2. } \new Voice { \voiceTwo f2 ef4 } >> \oneVoice
  af,4 <af c ef>4 q <f c' ef>2 <f bf df>4
  c'4 <af ef c> q <g ef> <af g ef> <bf g ef>
  <c af ef>4 <bf gf ef> <af gf c,> <af f df>2 <f df>4

  <c ef af>2 q4 <ef g bf>2 q4
  <f af c>2 <f af>4 <f af bf>2 q4
  <ef af c>2 q4 <f g bf>2 q4
  <df gf af>2 <df gf bf>4 <f af>2 <f af df>4
  <af c ef>2 q4 <gf bf df>2 <gf bf c>4
  <df f bf>4 <df f af>2 <df ff af>2 <df ff bf>4
  <ef af c>2 q4 <df gf af>2 <df gf bf>4
  <df f af>2 q4 <df ff af>2 df4

  <af' ef c>4 q q <g f bf,>2 r4
  r4 <af f c> <bf f c> <c f, df> <f, df> bf,
  <af' ef c>4 q q << { \voiceOne af2 g4 } \new Voice { \voiceTwo <f bf,>2. } >> \oneVoice
  r4 <af f c> <bf f c> <c f, df>2 <df bf f>4
  <af df ef>2. ~ <af bf df ef>2. ~
  <af bf c ef>1. ~ <af c ef>2. ~ 2
}

left = \relative c, {
  \global
  c4
  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. af2 g4

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. df2 ef4

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. af2 g4

  f2. g2.
  af2. bf2.
  f2. g2.
  af2. g2.
  f1.
  f1.
  ef2. df2.
  c2. c2 c4

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. df2 ef4

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. af2 g4

  f2. f2
  << { \voiceOne
    r4
    <g' bf>4 <f af> <ef g> <df f> <ef g> <f af>
    g2. r
  } \new Voice { \voiceTwo
    c,,4_\markup{ \italic {Ped.} }
    <f c'>1.
    c1.
  } >> \oneVoice
  \ottava #-1 \set Staff.ottavation = \markup { \concat { "8" \raise #0.5 \tiny "vb" } }
  c2.^\markup{ \italic {Man.} } ~ 2 c4
  f2. f4 c f
  g2 g,4 bf2 df4
  c1. ~
  2. ~ 2 4
  \ottava #0

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2. df2 ef4

  f2. f2 c4
  f2. df2 ef4
  f2. f2 c4
  f2 af4 df,2 f4

  af2. ef
  f df
  af'2. ef
  gf df
  af'2. gf
  df1.
  af'2. gf
  df2. ~ 2 4

  af'2. ef2 4
  f2 ef4 df2 f4
  af2. ef2 4
  f2 ef4 df2 ef4
  af2 af4 ef2 ef4
  af2 af4 f2 g4
  af2. ef
  af df,4 f df

  af'2. ef
  f df
  af'2. ef
  gf df
  af'2. gf
  df1.
  af'2. gf
  df2. ~ 2 4

  af'2. ef2 4
  f2 ef4 df2.
  af'2. ef
  f2 ef4 df2.
  af'4 4 4 ef2.
  af4 4 4 ef2 ef4
  <af ef'>2. ~ 2
}


choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
    \new Voice { \dynamicUp \saControl }
  >>
  \new Lyrics = "soprano-lyrics" \with {
    alignAboveContext = "sa"
    \override VerticalAxisGroup.staff-affinity = #DOWN
  } \lyricsto "soprano" \sopranoVerse
  \new Lyrics = "alto-lyrics" \lyricsto "alto" \altoVerse
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
    \new Voice { \dynamicDown \tbControl }
  >>
  \new Lyrics = "tenor-lyrics" \with {
    alignAboveContext = "tb"
    \override VerticalAxisGroup.staff-affinity = #DOWN
  } \lyricsto "tenor" \tenorVerse
  \new Lyrics = "bass-lyrics" \lyricsto "bass" \bassVerse
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
