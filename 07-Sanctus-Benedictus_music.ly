\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key a \minor
  \time 2/2
  \tempo "Misterioso" 2=70
}

tmControl = {
  s1*8 \bar"||"\mark\default\break
  s1*16 \bar"||"\mark\default
  s1*8 \bar"||"\mark\default\break
  s1*8 \bar"||"\mark\default
  s1*16 \bar"||"\mark\default
  s1*8 \bar"||"\mark\default
  s1*8 \bar"||"\mark\default
  s1*8 \bar"||"\mark\default
  s1*13 \bar"|."
}

pnControl = {
  s1\mf
  s1*3
  s1\mp
  s1*3 \bar"||"
  s1\p
  s1*7
  s1\mp
  s1*7 \bar"||"
  s1\<
  s1\mf
  s1\mp\cresc
  s1*3
  s1\f
  s1\mp \bar"||"
  s1\p\cresc
  s1*5
  s1\mf
  s1\< \bar"||"
  s1\f
  s1*9
  s1\dim
  s1*5 \bar"||"
  s1\mp
  s1*7 \bar"||"
  s1\<
  s2.\mf s4\mp\cresc
  s1*4
  s1\f
  s1\mp \bar"||"
  s1\p\cresc
  s1*7 \bar"||"
  s1\f
  s1*9
  s1\<
  s1
  s1\ff \bar"|."
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1*8

  \set Staff.shortInstrumentName = #"S."
  r2 c\p ~
  c a
  r2 c ~
  c b
  r2 c ~
  c a
  c( b4 c
  a1)
  \set Staff.shortInstrumentName = #""
  r2 c4(\mp d
  e2) a,
  r2 c4( d
  c2) b
  r2 c4( d
  e2) a,
  c( b4 c
  a1)

  r2 c4\< a
  d4\mf a2.
  r2 c4\mp\cresc a
  d4 c2 d4
  e1 ~
  e1 ~
  e1\f
  R1

  a,4\p a2 g4
  a2. r4
  c4\mp c2 b4
  c2 d2
  b1\< ~
  b2 b4 c
  d2\mf a2 ~
  a2 r4

  d4\f
  e4 c2 d4
  e4 c2.
  R1
  r2. d4
  e4 c2 d4
  e4 c2.
  R1
  r2. b4
  c1
  a1
  R1*6

  r2 c\mp ~
  c a
  r2 c ~
  c b
  r2 c4( d
  e2) a,
  c b4( c)
  a1

  r4 a4\< c a
  d4(\mf a2.)
  r4 a4\mp\cresc c a
  d2 c4 d4
  e1
  d2 c
  b1\f
  R1

  a4\p\cresc a2 g4
  a2 r4 b
  c4 c2 b4
  c2 r4 d4
  b4 b2 a4
  b2 b4 c
  d1

  r2.\! d4\f
  e4 c2 d4
  e4 c2.
  R1
  r2. d4
  e4 c2 d4
  e4 c2.
  R1
  r2. d4
  e4 c2 d4
  e4 c2.
  e2\< e2
  e2 e4 b4
  c4\ff a r2
}

alto = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1*8

  \set Staff.shortInstrumentName = #"A."
  R1
  a8(\p g) a2. ~
  2 r
  a8( g) a2. ~
  2 r
  a8( g a2) e4
  f2( g
  a1)
  \set Staff.shortInstrumentName = #""
  R1
  a8(\mp g) a2. ~
  2 r
  a8( g) a2. ~
  2 r
  a8( g a2) e4
  f2( g
  a1)

  r4 a2\< g4
  fs4\mf fs2.
  r4 a2\mp\cresc g4
  fs4 fs2 fs4
  g1( ~
  g1
  gs1)\f
  R1

  R1
  e4\p e2 d4
  e2. r4
  e4\mp e2 f4
  g2. g4
  g2\< g4 a
  fs2\mf fs2 ~
  2 r4

  d4\f
  e4 c2 d4
  e4 e2.
  gs2 b
  a gs
  r2.d4
  e4 e2.
  gs2 b
  a gs4 b
  c1
  a1
  R1*6

  R1
  a8(\mp g) a2. ~
  2 r
  a8( g) a2. ~
  2 r
  a8( g a2) e4
  f2 g
  a1

  r4 a\< a g
  fs1\mf
  r4 a\mp\cresc a g
  fs2 fs4 fs
  g1
  g2 g
  gs1\f
  R1

  R1
  e4\p\cresc e2 d4
  e2 r4 e4
  e4 e2 f4
  g2. g4
  g2 g4 a
  fs1

  r2.\! d4\f
  e4 c2 d4
  e4 e2.
  gs2 b
  a gs
  r2. d4
  e4 e2.
  gs2 b
  a gs
  r2.d4
  e4 e2.
  gs2\< b
  a gs4 b
  c4\ff a r2
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8

  \set Staff.shortInstrumentName = #"T."
  e2\p a,~
  2 r
  f'2 c ~
  2 r
  e1(
  a,2) e'
  f2.( e4
  d2 c)
  \set Staff.shortInstrumentName = #""
  e2\mp a, ~
  2 r
  f'2 c ~
  2 r
  e1(
  a,2) e'
  f2.( e4
  d2 c)

  r2 c4\< c
  d4\mf d2.
  r2 c4\mp\cresc c
  d4 d2 d4
  c1( ~
  c1
  b)\f
  R1

  R1
  c4\p c2 a4
  c2. r4
  g4\mp g2 b4
  d2. d4
  d2\< b4 a4
  a2\mf d2 ~
  2 r4

  d4\f
  e4 c2 d4
  e4 c2.
  b2 d
  c b
  r2. d4
  e4 c2.
  b2 d
  c b4 b
  c1
  a1
  R1*6

  e'2\mp a, ~
  2 r
  f'2 c ~
  2 r
  e1(
  a,2) e'
  f2. e4
  d2( c)

  r4 c\< c c
  d1\mf
  r4 c\mp\cresc c c
  d2 d4 d
  c1
  c2 c
  e1\f
  R1

  R1
  c4\p\cresc c2 a4
  c2 r4 b4
  g4 g2 b4
  d2. d4
  d2 b4 a4
  a1

  r2.\! d4\f
  e4 c2 d4
  e4 c2.
  b2 d
  c b
  r2. d4
  e4 c2.
  b2 d
  c b
  r2. d4
  e4 c2.
  b2\< d
  c b4 b
  c4\ff a r2
}

bass = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8

  \set Staff.shortInstrumentName = #"B."
  a1\p
  a
  f
  f
  a
  a
  f ~
  f
  \set Staff.shortInstrumentName = #""
  a1\mp
  a
  f
  f
  a
  a
  f ~
  f

  a2\< a,
  d4\mf d2.
  a'2\mp\cresc a,
  d4 d2 d4
  c1( ~
  c2 d2
  e1)\f
  R1

  a4\p a2 e4
  a,2. r4
  c4\mp c2 g4
  c2. d4
  g,1\<
  g'2 f4( e)
  d2\mf d2 ~
  2 r4

  d4\f
  c4 c2 g4
  c4 c2 d4
  e4 e2 b4
  e4 e d2
  c4 c2 g4
  c4 c2.
  e2 e
  e2 gs4 e
  a1
  a,1
  R1*6

  a'1\mp
  a
  f
  f
  a
  a
  f2 f
  f2. 4\<

  a2 a,
  d2.\mf d4\mp\cresc
  a'2 a,
  d2 d4 d
  c1
  c2 d2
  e1\f
  R1

  a4\p\cresc a2 e4
  a,2 r4 b4
  c4 c2 g4
  c2 r4 d4
  g,4 g2 g4
  g'2 f4 e
  d1

  r2.\! d4\f
  c4 c2 g4
  c4 c2 d4
  e4 e2 b4
  e4 e2 b4
  c4 c2 g4
  c4 c2 d4
  e4 e2 b4
  e4 e d2
  c4 c2 g4
  c4 c2.
  e2\< e
  e2 gs4 b
  c4\ff a r2
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  God of pow -- er,
  God of pow -- er and might. __

  Hea -- ven and earth,
  hea -- ven and earth
  are full of your glo -- ry.

  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!

  Bles -- sed, bles -- sed, bles -- sed is the one,

  the one who comes,
  the one who comes,
  in the name of the Lord.

  Blest is the one
  who comes in the name,
  who comes in the name of the Lord.

  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na!
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  God of pow -- er,
  God of pow -- er and might. __

  Hea -- ven and earth,
  hea -- ven and earth
  are full of your glo -- ry.

  Ho -- san -- na!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na!

  Bles -- sed, bles -- sed, bles -- sed is the one,

  the one who comes,
  the one who comes,
  in the name of the Lord.

  Blest is the one
  who comes in the name,
  the name of the Lord.

  Ho -- san -- na!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na!

}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  God of pow -- er,
  God of pow -- er and might. __

  Hea -- ven and earth,
  hea -- ven and earth
  are full of your glo -- ry.

  Ho -- san -- na!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na!

  Bles -- sed, bles -- sed, bles -- sed is the one,

  the one who comes,
  the one who comes,
  in the name of the Lord.

  Blest is the one
  who comes in the name,
  the name of the Lord.

  Ho -- san -- na!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na in the high -- est!
  Ho -- san -- na!
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  Ho -- ly, ho -- ly, ho -- ly Lord, __
  God of pow -- er,
  God of pow -- er and might. __

  Hea -- ven and earth,
  hea -- ven and earth
  are full of your glo -- ry.

  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na in the high -- est,
  Ho -- san -- na!

  Bles -- sed, bles -- sed, bles -- sed is the one,

  the one who comes,
  the one who comes,
  in the name of the Lord.

  Blest is the one
  who comes in the name,
  who comes in the name of the Lord.

  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na!
  Ho -- san -- na in the high -- est,
  Ho -- san -- na!

}

right = \relative c' {
  \global
  r2 r4 <c>4 ~
  <c e a>1 ~
  <c e a>1
  <c d a'>2 <c f a>
  r2 r4 <c>4 ~
  <c e a>1 ~
  <c e a>1
  <c d a'>2 <c f a>

  e2 r4
  << { \voiceTwo
    c4 ~
    c1 ~
    1
  } \new Voice { \voiceOne
    s4
    a'1 ~
    1
  } \new Voice { \voiceThree
    s4
    e1
    f2 e
  } >> \oneVoice
  <c d a'>2 <c f b>
  r2 r4
  << { \voiceTwo
    c4 ~
    c1 ~
    c1
  } \new Voice { \voiceOne
    s4
    a'1 ~
    a2 g
  } \new Voice { \voiceFour
    s4
    e1
    f2. e4
  } >> \oneVoice
  <c d a'>2 <c f a>

  << { \voiceOne
    r2 c'4( d
    <e a,>2) a, ~
    2 <c a>4( <d b>
    <c a>2) b
    r2 c4( d
    <e a,>2) a,
    <a c>2 << { b4 c } \new Voice { \voiceThree g2 } >>
  } \new Voice { \voiceTwo
    r2 e2 ~
    e1
    f2 e
    d2 f
    r2 e2 ~
    e1
    f2. e4
  } >> \oneVoice
  <c d a'>2 <c f a>

  r4 <a'> ~ <a c> ~ <e a c>
  <fs a d>1
  r4 <a> ~ <a c> ~ <e a c>
  <fs a d>1
  <g c e>1
  <e g d'>2 <e g c>
  <gs b e>1
  e4 gs b d

  e4 c a e
  a c e c
  e c g e
  g c e c
  d b g d
  g b d e
  fs d a fs
  a d fs d

  <e c g>4 <c g>2 <d g,>4
  <e c g>4 <c g>2 <d g,>4
  <e b gs>2 <e d b>
  <e c a>2 <e b gs>
  <e c g>4 <c g>2 <d g,>4
  <e c g>4 <c g>2 <d g,>4
  <e b gs>2 <e d b>
  <e c>2 <gs e b>

  <a e c>2 r4 <c,>4 ~
  <c e a>1 ~
  <c e a>1
  <c d a'>2 <c f a>
  r2 r4 <c,>4 ~
  <c e a>1 ~
  <c e a>1
  <c d a'>2 <c f a>

  e2 r4
  << { \voiceTwo
    c4 ~
    c1 ~
    1
  } \new Voice { \voiceOne
    s4
    a'1 ~
    1
  } \new Voice { \voiceThree
    s4
    e1
    f2 e
  } >> \oneVoice
  <c d a'>2 <c f b>
  << { \voiceOne
    r2 c'4( d
    <e a,>2) a,
    <a c>2 << { b4 c } \new Voice { \voiceThree g2 } >>
  } \new Voice { \voiceTwo
    r2 e2 ~
    e1
    f2. e4
  } >> \oneVoice
  <c d a'>2 <c f a>

  r4 <a'> ~ <a c> ~ <e a c>
  <fs a d>1
  r4 <a> ~ <a c> ~ <e a c>
  <fs a d>1
  <g c e>1
  <e g d'>2 <e g c>
  <gs b e>1
  e4 gs b d

  e4 c a e
  a c e c
  e c g e
  g c e c
  d b g d
  g b d e
  fs d a fs
  a d fs d

  <e c g>4 <c g>2 <d g,>4
  <e c g>4 <c g>2 <c g>4
  <b gs>2 <d b>
  <c a>2 <b gs>
  <e c g>4 <c g>2 <d g,>4
  <e c g>4 <c g>2 <c g>4
  <b gs>2 <d b>
  <e c a>2 <e b gs>
  <e c g>4 <c g>2 <d g,>4
  <e c g>4 <c g>2 <d g,>4
  <e b gs>2 <e d b>
  <e c>2 <gs e b>
  <a c,>4 <a a,>4 r2
}

left = \relative c {
  \global
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1

  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1

  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1

  <a a'>1
  <d, a' d>2. q4
  <a' a'>1
  <d, d'>2. q4
  c'2 g
  <c c,>2 <d d,>
  <e e,>1
  R1

  << { \voiceTwo
    a,2_\markup{ \italic { Ped. } } e
    a a4 b
    c2 g
    c c4 d
    g,1 ~
    g2 f4 e
    d1
    d'1
    c2 2
    2 2
    e,1 ~
    1
    c'2 2
    c2 2
    e,1 ~
    1
  } \new Voice { \voiceOne
    <a' c>1 ~
    q1
    <g c>1 ~
    q1
    <g b>1 ~
    q1
    <a d>1 ~
    q1
    g1 ~
    g1
    e2 2
    2 d
    g1 ~
    g1
    e2 2
    e2 2
  } >> \oneVoice


  <a,>4_\markup{ \italic { Man. } } ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1
  <a>4 ~ <a e'> ~ <a e' a>2 ~
  q1
  <f>4 ~ <f c'> ~ <f c' f>2 ~
  q1

  <a a'>1
  <d, a' d>2. q4
  <a' a'>1
  <d, d'>2. q4
  c'2 g
  <c c,>2 <d d,>
  <e e,>1
  R1

  << { \voiceTwo
    a,2_\markup{ \italic { Ped. } } e
    a a4 b
    c2 g
    c c4 d
    g,1 ~
    g2 f4 e
    d1
    d'1
    c2 2
    2 2
    e,1 ~
    1
    c'2 2
    2 2
    e,1 ~
    1
    c'2 2
    c2 2
    e,1 ~
    1
  } \new Voice { \voiceOne
    <a' c>1 ~
    q1
    <g c>1 ~
    q1
    <g b>1 ~
    q1
    <a d>1 ~
    q1
    g1 ~
    g1
    e2 2
    2 2
    g1 ~
    g1
    e2 2
    2 d
    g1 ~
    g1
    e2 2
    e2 2
  } >> \oneVoice
  <a a,>4 q r2
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
