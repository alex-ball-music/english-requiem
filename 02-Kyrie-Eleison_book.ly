\version "2.20.0"
\include "02-Kyrie-Eleison_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
