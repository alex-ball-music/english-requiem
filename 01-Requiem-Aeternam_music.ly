\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key c \major
  \time 2/2
  \tempo "Solenne" 2=60
}

tmControl = {
  s1*10 \bar"||"\mark\default
  s1*15
  s2 \breathe s2
  s1*12 \bar"||" \tempo "Poco più mosso"
  s1*7 \bar"||"\mark\default
  s1*2 \break
  s1*16 \bar"||"\mark\default \tempo "A tempo"
  s1*17
  s2 \tempo "Rubato" s2 \bar"||"\mark\default
  \override TextSpanner.bound-details.left.text = "tenuto"
  s2. s4\startTextSpan
  s2. s4\stopTextSpan
  s2. s4\startTextSpan
  s2. s4\stopTextSpan
  \override TextSpanner.bound-details.left.text = "rall."
    s4\startTextSpan
  s1
  s2. s4\stopTextSpan
  s1*2
  \bar"|."
}

tempotrack = {
  s1*38
  \tempo 2=64
  s1*25
  \tempo 2=60
  s1*17
  s2
  \tempo 2=56
  s2
  % D
  \tempo 2=52
  s2
  \tempo 2=48
  s2
  \tempo 2=44
  s2
  \tempo 2=40
  s4
  \tempo 2=44
  s4
  % D+2
  \tempo 2=52
  s2
  \tempo 2=48
  s2
  \tempo 2=44
  s2
  \tempo 2=40
  s4
  \tempo 2=44
  s4
  % D+4
  \tempo 2=52
  s2
  \tempo 2=48
  s2
  \tempo 2=44
  s1
  \tempo 2=40
  s1*2
}

saControl = { \override VerticalAxisGroup.remove-layer = #3
  \set Staff.shortInstrumentName = \markup \center-column { "S." "A." }
  s1*10 \bar"||"
  s1*8 \override VerticalAxisGroup.remove-layer = #2
  \set Staff.shortInstrumentName = #""
  s1\mp
  s1*5
  s1\<
  s2 \breathe s2
  s1\f
  s1*6
  s2 s2\>
  s1\mp
  s1*3 \bar"||"
  s1*7 \bar"||"
  s1*8
  s2 s2\mf
  s1*4
  s1\<
  s1
  s1\f
  s1*2 \bar"||"
  s1*2 \splitStaffBarLine
}

tbControl = {
  s1*10 \bar"||"
  \set Staff.shortInstrumentName = \markup \center-column { "T." "B." }
  s1\p
  s1*7
  \once\override DynamicText.self-alignment-X = #RIGHT
  \set Staff.shortInstrumentName = #""
  s1\mp
  s1*5 \dynamicDown
    s1\<
  s2 \breathe s2
  s1\f
  s1*6
  s2 s2\>
  s1\mp
  s1*3 \bar"||"
  s1*7 \bar"||"
  s1*8
  s2 s2\mf
  s1*4 \dynamicUp
  s1\<
  s1
  s1\f
  s1*2 \bar"||"
  s1*2 \splitStaffBarLine
}

pnControl = {
  s1\f\>
  s1
  s1\mp
  s1*6
  s1\> \bar"||"
  s1\p
  s1*5
  s1\<
  s1
  s1\mp
  s1*5
  s1\<
  s1
  s1\f
  s1*6
  s2 s2\>
  s1\mp
  s1*3 \bar"||"
  s1*7 \bar"||"
  s1*7
  s1\<
  s1
  s1\mf
  s1*3
  s1\<
  s1
  s1\f
  s1\>
  s1 \bar"||"
  s1\p
  s1*5
  s1\cresc
  s1*3
  s1\f
  s1*7 \bar"||"
  s1*8 \bar"|."
}


sopranoPI = \relative c' {
  \global
  % Music follows here.
  R1*18

  d2 d4 d
  f f g g
  d1
  R1
  d2 d4 d
  f f g g
  a1 ~
  a2

  a4 b
  c1
  b4. b8 a a g g
  a2 d,
  r a'4 b
  c1
  c4. c8 c c d d
  e2 c ~
  c c4 b
  a2 a4 a
  bf bf a a
  g2 g
  R1

  \key f \minor
  R1*7
  g2 g
  af2 af4 af
  bf2 af
  g c,
  r r4 g'
  af4 f af g
  af c bf af
  g1

  \key c \major
  r2 g2
  a2 a
  a4 a2 g4
  a1
  r2 a2
  a a
  a4 a2 b4
  cs1
  R1*4
}

sopranoPII = \relative c' {
  \dynamicUp
  d2\p	 d4 d
  f f g g
  d1
  R1
  a'2\mp a4 a
  b b g f
  a1\< ~
  a2

  a4 b
  c1\f
  b4. b8 a a g g
  a2 d, ~
  d2 a'4 b
  c2 c4. c8
  b4. b8 a4 g
  a2 a ~

  a4 r4 d4 c
  a2 f4 a
  b4. g8 d'4 c
  a2 f4 a
  b4. g8 d'4 c8( b)
  a4.( g8) f4 a
  g4. a8 b4 g
  a2 2 ~
  1
}

altoPI = \relative c' {
  \global
  % Music follows here.
  R1*18

  d2 d4 d
  d d d d
  d1
  R1
  d2 d4 d
  d d d e
  f1(
  e2)

  e4 g
  a1
  g4. g8 f f e e
  f4( e) d2
  r f4 g
  a1
  a4. a8 a a b b
  c2 c4( b
  a2) a4 g
  f2 f4 f
  f f f f
  e2 e
  R1

  R1*7
  g2 g
  af2 af4 af
  bf2 af
  g c,
  r r4 g'
  af4 f af g
  af c bf af
  g1

  r2 g2
  f2 f
  f4 f2 f4
  e1
  r2 e2
  f2 f
  g4 g2 g4
  a1
  R1*4
}

altoPII = \relative c' {
  \dynamicUp
  R1*2
  d2\p d4 d
  f f g g
  d1
  d2\mp d
  f2\< f
  e2

  e4 g
  a1\f
  g4. g8 f f e e
  f4( e) d2 ~
  d2 f4 g
  a2 a4. a8
  g4. g8 f4 e
  f4( e) d2 ~

  d4 r4 r2
  f4( e) d f
  g4. g8 g4 r
  f4( e) d f
  g4. g8 g4 r
  f4( e) d f
  g4. g8 g4 f
  e2 2 ~
  1
}

tenorPI = \relative c' {
  \global
  % Music follows here.
  R1*8
  R1^\markup{ \smcp { tenors } and \smcp { basses } }
  R1

  a2. a4
  g2 g
  a1
  R1
  a2. a4
  b2 g4( f)
  a1
  R1

  a2. a4
  g2 b
  a1
  R1
  a2 a4 a
  g g b b
  a2( b
  cs2)

  cs4 d
  f( e d c)
  b4. d8 d d b b
  a2 a
  r a4 d
  e( d c e)
  f4. f8 f f f f
  g2 e2 ~
  e e4 e
  c2 c4 c
  d d c c
  d2 c
  R1

  \key f \minor
  R1*7
  g2 g
  af2 af4 af
  bf2 af
  g c,
  r r4 g'
  af4 f af g
  af c bf af
  g1

  \key c \major
  r2 c2
  c2 c
  d4 d2 d4
  cs1
  r2 cs
  c2 c
  d4 d2 d4
  e1
  R1*4
}

tenorPII = \relative c' {
  \dynamicUp
  a1\p
  g2 b
  a2 a
  g2 g
  a2 a2\mp
  g2 b2
  a2\< b
  cs2

  cs4 d
  f(\f e d c)
  b4. d8 d4 b
  d4 d c( b)
  a2 a4 d
  f( e) d c
  b4. d8 d4 b
  a2 a ~

  a4 r4 r2
  r4 c c c
  b4. c8 d2
  r4 c c c
  b4. c8 d2
  r4 c d e
  d4. f8 e4 d
  cs2 2 ~
  1
}

bassPI = \relative c {
  \global
  % Music follows here.
  R1*10

  d2. d4
  d2 d
  d1
  R1
  d2. d4
  d2 d
  d1
  R1

  d2. d4
  d2 d
  d1
  R1
  d2 d4 d
  d d d d
  d2( g,
  a2)

  a'4 g
  f1
  g4. g8 g g e e
  f2 f
  r d4 d
  a1
  f4. f'8 f f f f
  c2 c4( d
  e2) c4 c
  f2 f4 f
  d d f f
  c2 c
  R1

  R1*7
  g'2 g
  af2 af4 af
  bf2 af
  g c,
  r r4 g'
  af4 f af g
  af c bf af
  g1

  r2 g2
  f2 f
  d4 d2 d4
  a1
  r1
  r4 a b c
  d e( f) g
  a1
  R1*4
}

bassPII = \relative c {
  \dynamicUp
  d1\p
  d2 d
  d2 d
  d2 d
  d2 d\mp
  d2 d
  d2\< g,
  a2

  a'4 g
  f1\f
  g4. g8 g g e e
  f2 f ~
  f f4 g
  f2 f4. f8
  g4. g8 g4 e
  f2 f ~

  f4 r4 g4 e
  f1
  g2 g4 e
  f1
  g2 g4 e
  f2 d4 d
  g g g b
  a2 2 ~
  1
}

sopranoVerseII = \lyricmode {
  % Lyrics follow here.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord:
  And may light shine per -- pe -- tual -- ly up -- on them, __
  And may light shine per -- pe -- tual -- ly up -- on them,
  And may light shine per -- pe -- tual -- ly,
  May light shine per -- pe -- tual -- ly,
  May light shine per -- pe -- tual -- ly up -- on them. __
}

altoVerseI = \lyricmode {
  % Lyrics follow here.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord:
  And may light shine per -- pe -- tual -- ly up -- on them,
  And may light shine per -- pe -- tual -- ly up -- on them,
  And may light shine per -- pe -- tual -- ly up -- on them.

  Praise be -- fits you, O God, in Zi -- on,
  Our pro -- mi -- ses to you will be ful -- filled:
  To you who lis -- ten to prayer,
  To you all mor -- tals shall come.
}

altoVerseII = \lyricmode {
  % Lyrics follow here.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them rest, O Lord.
  And may light shine per -- pe -- tual -- ly up -- on them, __
  And may light shine per -- pe -- tual -- ly up -- on them,
  Light shine per -- pe -- tual -- ly,
  Light shine per -- pe -- tual -- ly,
  Light shine per -- pe -- tual -- ly up -- on them. __
}

tenorVerseII = \lyricmode {
  % Lyrics follow here.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest:
  And may light __ shine per -- pe -- tual -- ly up -- on __ them,
  And may light __ shine per -- pe -- tual -- ly up -- on __ them,
  Light shine per -- pe -- tual -- ly,
  Light shine per -- pe -- tual -- ly,
  Light shine per -- pe -- tual -- ly up -- on them. __
}

bassVerseI = \lyricmode {
  % Lyrics follow here.
  Rest, e -- ter -- nal rest.
  Rest, e -- ter -- nal rest.
  Rest, e -- ter -- nal rest.

  \repeat unfold 67 { \sh }
  To you all mor -- tals shall come.
}

bassVerseII = \lyricmode {
  % Lyrics follow here.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest:
  And may light shine per -- pe -- tual -- ly up -- on them, __
  And may light shine per -- pe -- tual -- ly up -- on them, __
  And may light shine,
  And may light shine,
  And may light shine per -- pe -- tual -- ly up -- on them. __
}

right = \relative c'' {
  \global
  % Music follows here.
  R1*2
  <f a>4 d <e g> c
  <d f> b <c e> a
  <d f> a <c e> a
  <b d> g <a c> f
  <a d> f <a c> e
  <g b> d <f a> d
  <e g> d f4 e
  d1 ~

  d1
  R1
  r2 d4 e
  f g e c
  d1
  R1
  a'4 g f e
  f e d c

  d1
  f2 g
  d2 a'4 b
  c d b g
  a2 d,
  <d f> << { \voiceOne
    g <f a>1 ~
  } \new Voice { \voiceTwo
    d4 e a,2 b
  } >> \oneVoice
  <cs e a>2

  <cs e a>4 <d g b>4
  << { \voiceOne
    <a' c>1
  } \new Voice { \voiceTwo
    f4 e d c
  } >> \oneVoice
  <d g b>2 <d e g>2
  << { \voiceOne
    a'2 d, ~
    d2
  } \new Voice { \voiceTwo
    f4 e c b
    a2
  } >> \oneVoice
  <f' a>4 <d g b>
  << { \voiceOne
    <a' c>1
  } \new Voice { \voiceTwo
    e4 d c e
  } >> \oneVoice
  <f a c>2 q4 <f b d>
  <g c e>2
  << { \voiceOne
    <e c'>2 ~
    q2
  } \new Voice { \voiceTwo
    c'4 b
    a g
  } >> \oneVoice
  <e a c>4 <e g b>
  <f a>2 q
  <f bf>2 <f a>
  <d e g> <c e g>
  <c d g> <c e g>

  \key f \minor
  << { \voiceOne
    af'2 c
    bf af
    f2 e
    d e
    af2 c
    bf af
  } \new Voice { \voiceTwo
    <c, f>1 ~
    q1 ~
    <c g'>1 ~ \once \override TieColumn.tie-configuration = #'((0.0 . 1) (-7.5 . -1))
    q1 ~
    <c f>1 ~
    q1
  } >> \oneVoice
  <f g c>1
  <e g>
  << { \voiceOne
    af4 bf c df
    c f ef df
    c2 bf
    af g
    af4 bf c df
    c f ef df
    e1
    \key c \major
    c2 b
  } \new Voice { \voiceTwo
    f1 ~
    f1
    e1 ~
    e1
    f2. g4
    af1
    g1
    e1
  } >> \oneVoice
  <f a>2 q
  <d f a>4 q2 <d f g>4
  <cs e a>1
  <cs e a>2 <cs e a>2
  <c f a>2 q
  <d g a>4 q2 <d g b>4
  <e a cs>1 ~
  <e a cs>4 ~ <e g b> <f a> <e g>
  <d f> <c e> <a d> <a c>

  d1 ~
  d1
  d1
  f2 g
  d1
  f2 g
  <a d,>1
  << { \voiceOne
    b2 g4 f
    <f a>1
  } \new Voice { \voiceTwo
    d1
    a2 b
  } >> \oneVoice
  <cs e a>2

  <cs e a>4 <d g b>4
  << { \voiceOne
    <a' c>1
  } \new Voice { \voiceTwo
    f4 e d c
  } >> \oneVoice
  <d g b>2 <d e g>2
  << { \voiceOne
    a'2 d, ~
    d2
  } \new Voice { \voiceTwo
    f4 e c b
    a2
  } >> \oneVoice
  <f' a>4 <d g b>4
  << { \voiceOne
    <a' c>1
  } \new Voice { \voiceTwo
    f4 e d c
  } >> \oneVoice
  <d g b>2 <d e g>2
  << { \voiceOne
    a'2
  } \new Voice { \voiceTwo
    f4 e
  } >> \oneVoice
  <a d,>2
  <a f>4 <c a> <d b> <c e>
  << { \voiceTwo
    <a' c,>1
  } \new Voice { \voiceOne
    f4 <e g> <d f> e
  } >> \oneVoice
  <b d>2 <b d e g>
  << { \voiceTwo
    <a' c,>1
  } \new Voice { \voiceOne
    f4 <e g> <d f> e
  } >> \oneVoice
  <b d>2 <b d e g>
  << { \voiceTwo
    <a' c,>1
  } \new Voice { \voiceOne
    f4 <e g> <d f> <e f>
  } >> \oneVoice
  <b d e g>1
  <a cs e a>1 ~
  q
}

left = \relative c {
  \global
  % Music follows here.
  <d a d,>1 ~
  <d a d,>1 ~
  <d a d,>1 ~
  <d g, d>1 ~
  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    b2 g4( f)
  } >>
  <d' a d,>1 ~
  <d g, d>1 ~
  <d a d,>1 ~
  << {
    <d d,>1
  } \new Voice { \voiceTwo
    g,2 b
  } >>

  <d a d,>1 ~
  <d g, d>1 ~
  <d a d,>1 ~
  <d a d,>1 ~
  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    b2 g4( f)
  } >>
  <d' a d,>1 ~
  <d a d,>1 ~

  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    g,2 b
  } >>
  <d a d,>1 ~
  <d a d,>1 ~
  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    g,2 b
  } >>
  <d a d,>2 <d g,>
  <e a,>

  a4 g
  f1
  << { \voiceTwo
    g2. e4
  } \new Voice { \voiceOne
    b'1
  } >> \oneVoice
  <d, f>1
  f4 e <d a'>2
  <a a'>1
  <f f'>1
  c'2. d4
  e2 c
  <f c'>1
  <d d'>2 <f c'>
  <c g'>1 ~
  <c g'>1

  \key f \minor
  <f>1 ~
  <f f,>1
  <c g'>1 ~
  <c g'>1
  <f>1 ~
  <f f,>1
  <c g'>1 ~
  <c g'>1
  << { \voiceTwo
    f,1_\markup{ \italic { Ped. } } ~
    f1
    c'1
    c,1
    f1 ~
    f1
    c'2_\markup{ \italic { Man. } } d
    \key c \major
    e1
  } \new Voice { \voiceOne
    <f af>2 q4 q
    <f bf>2 <f af>
    g1 ~
    g2. <e g>4
    <f af>4 f <e af> <e g>
    <f af> <af c> <g bf> <f af>
    g1 ~
    g1
  } >> \oneVoice
  <f c'>2 q
  d4 d2 d4
  a4 a' g f
  e f e a,
  f a b c
  d e f g
  << { \voiceTwo <e a,>1 ~ } \new Voice { \voiceOne a4 e f g } >> \oneVoice
  <a e a,>1
  a2 a,

  d,1 ~
  <d' a d,>1
  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    g,2 b
  } >>
  <d a d,>1 ~
  <d g, d>1 ~
  <d a d,>1 ~
  << {
    <d d,>1 ~
  } \new Voice { \voiceTwo
    g,2 b
  } >>
  <d a d,>2 <d g,>
  <e a,>

  a4 g
  f1
  << { \voiceTwo
    g2. e4
  } \new Voice { \voiceOne
    b'1
  } >> \oneVoice
  <d, f>1
  f4 e << { \voiceTwo d2 } \new Voice { \voiceOne <f a>4 g } >> \oneVoice
  f1
  << { \voiceTwo
    g2. e4
  } \new Voice { \voiceOne
    b'1
  } >> \oneVoice
  <f a>2 << { \voiceTwo
    d2 ~
    d2 d4 e
  } \new Voice { \voiceOne
    f4 <g b>
    <f a>2 <g b>
  } >> \oneVoice
  <f f,>1
  <g g,>2. <a a,>4
  <f f,>1
  <g g,>2. <a a,>4
  <f f,>2 <d d,>2
  <d g,>2 <e b>
  << { \voiceOne
    <e a>1 ~ 1
  } \new Voice { \voiceTwo
    r2 a,2 1
  } >>
}

PI = { \global s1*65 }

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }

  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano1" { \voiceOne \sopranoPI }
    \new Voice = "alto1" { \voiceTwo \altoPI }
    \new Voice { \dynamicUp \saControl }
  >>
  \new Lyrics = "sa-lyrics" \lyricsto "alto1" \altoVerseI
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano2" { \PI \sopranoPII }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano2" { \sopranoVerseII }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto2" { \PI \altoPII }
  \new Lyrics = "alto-lyrics" \lyricsto "alto2" { \altoVerseII }

  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
    \override VerticalAxisGroup.remove-layer = 2
  } <<
    \clef bass
    \new Voice = "tenor1" { \voiceOne \tenorPI }
    \new Voice = "bass1" { \voiceTwo \bassPI }
    \new Voice { \dynamicUp \tbControl }
  >>
  \new Lyrics = "tb-lyrics" \lyricsto "bass1" \bassVerseI
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor2" { \clef "treble_8" \PI \tenorPII }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor2" \tenorVerseII
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass2" { \clef bass \PI \bassPII }
  \new Lyrics = "bass-lyrics" \lyricsto "bass2" \bassVerseII
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
