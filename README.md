# An English Requiem for the Commemoration of the Faithful Departed

My local Anglican parish church holds a Requiem mass each year to celebrate
All Souls' Day, officially the Commemoration of the Faithful Departed. One year,
our Choir sang a popular setting of the Latin Mass (though not a Requiem) for
this service and it went so well we repeated it on subsequent years. This was
lovely, but after five or more years of singing the same thing, we fancied
a change.

At our annual meeting at the beginning of 2020, we were discussing how hard it
was to find a suitable (and affordable!) replacement. One of my fellow
choristers, knowing my interest in composition, nudged me and suggested I should
have a go at writing one. I was a bit sceptical, and in any case I was starting
work on a setting for Evensong, but the idea stayed with me. I finally started
thinking seriously about it in April and began putting ideas together in June.

There are of course many superb settings of the Requiem texts, so in order to
get a fresh take on them I quickly decided that I would take the unusual step of
setting the texts in modern English instead of Latin. The good thing about this
is that the congregation would understand all the words without having to have
a translation in front of them. But they would also get to hear all that stuff
about hell and torment unshielded by the language barrier.

In the texts I have chosen, I have tried to strike a balance between tradition
and the expectations of a modern congregation gathering to mourn the loss of
loved ones; and I have tried to be sparing in the amount of repetition of texts
between movements.

  - The Introit uses the traditional text (drawn from 2 Esdras 2:34–35 and
    Psalm 65:1-2).

  - The petitions added to the Kyrie come from ‘Kyrie Confession – Spirit’,
    used for the service for All Souls in the Common Worship *Times and Seasons*
    volume.

  - For my Gradual, I use *Absolve, Domine,* which is the traditional Tract.

  - For the Commemoration, I took inspiration from the traditional Gradual,
    which starts the same way as the Introit but then quotes Psalm 112.6b-7a.
    My version uses Psalm 112.4a,6,9b and Wisdom 3.1,2a,3b (being the first part
    of the canticle, the Song of the Righteous). The title comes from verse 6 of
    the psalm, which highlights the liturgical action it accompanies.

  - The Offertory is one of the traditional texts.

  - The *Sursum Corda, Sanctus, Benedictus,* and Acclamation are standard
    Eucharistic texts.

  - The *Agnus Dei* uses the traditional variant petitions for the Requiem.

  - *Lux Aeterna* is the traditional text for the distribution of Communion.

I did not feel that the *Dies Irae,* as powerful as it would be, would fit well
in our annual service. *Pie Iesu* is a combination of the end of the *Dies Irae*
and the end of the *Agnus Dei,* so to avoid repetition I left it alone. I also
decided against setting *In Paradisum* and *Libera Me* since strictly speaking
they belong to the burial rites, and are therefore better suited to a service
for an individual rather than All Souls.

I have tried to make the setting relatively simple to learn and sing, yet
prayerful and dramatic.

## Summary information

  - *Voicing:* Solo voice/SATB

  - *Notes on ambitus:*

  - *Instrumentation:* Organ

  - *Approximate total performance length:* 25 minutes

  - *Licence:* Creative Commons Attribution-NonCommercial 4.0 International
    Public Licence: <https://creativecommons.org/licenses/by-nc/4.0/>,
    with the following exception.

    ‘Kyrie Confession – Spirit’ from *Common Worship: Services and Prayers
    for the Church of England* is © The Archbishops’ Council 2000. Published by
    Church House Publishing. Used by permission.
    [rights@hymnsam.co.uk](mailto:rights@hymnsam.co.uk).

## Files

This repository contains the [Lilypond](http://lilypond.org) and
[LaTeX](https://www.ctan.org) source code.

For PDF, MIDI and MP3 downloads, see the [Releases
page](https://gitlab.com/alex-ball-music/english-requiem/-/releases).
