\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key f \minor
  \time 4/4
  \tempo "Espressivo" 4=80
}

tmControl = {
  s1*13 \bar"||"\mark\default
  s1*4 \break  s1*4 \break s1*7 \bar"||"\mark\default
  s1*8 \bar"||"\mark\default
  s1*5 \break s1*11 \bar"||"\mark\default
  s1*4
  s2. \tempo "Meno mosso" s4
  s1*2
  \override TextSpanner.bound-details.left.text = "rall."
  s4\startTextSpan s2.
  s2. s4\stopTextSpan\bar"|."
}

tempotrack = {
  s1*56
  s2. \tempo 4=72 s4
  s1*2
  \tempo 4=68
  s2
  \tempo 4=64
  s2
  \tempo 4=60
  s1
}

saControl = {
  s1*8
  \set Staff.shortInstrumentName = \markup \center-column { "S." "A." }
  s1*5
  \set Staff.shortInstrumentName = #""
  s4 \once\override DynamicText.self-alignment-X = #RIGHT s4\p s2
  s1*3

  s1*4
  s4 \once\override DynamicText.self-alignment-X = #RIGHT s4\mp s2
  s1*6
  s2

  \set crescendoText = \markup { \italic { cresc. poco a poco} }
  \set crescendoSpanner = #'text
  s2\<
  s1*3
  s2 s4\f s4
  s1
  s2 s4\p s4
  s1*2

  s4 \once\override DynamicText.self-alignment-X = #RIGHT s4\mp s2
  s1*3

  s1*11
  s2

  s8\< s4.
  s1*3
  s2. \breathe s4
  s1*3
  s1\ff
}

pnControl = {
  s4 s2.\mf
  s1*3
  s1\>
  s1\mp
  s1*22
  \set crescendoText = \markup { \italic { cresc. poco a poco} }
  \set crescendoSpanner = #'text
  s1\<
  s1*3
  s2 s2\f
  s4. s8\mf s2
  s1\>
  s1\p
  s1
  s1\mp
  s1*14
  s1\<
  s1*7
  s1\ff
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1*13
  r4 r8 af8 g4 g
  af4. r8 r2
  r4 r8 g8 af4 af
  g4. r8 r2

  R1*4
  r4 r8 af8 g4 g
  af4. r8 r2
  r4 r8 af8 g4 g
  af4. r8 r2
  R1*3
  r2 r8

  ef8 f g
  af2 af4 bf
  g2 ~ 8 g af bf
  c4 c2 c8 df
  bf2 c4 bf
  c4 c2.
  r2 af4 g
  af4 af2.

  R1
  r4 r8 af8 g4 g
  af4. r8 r2
  r4 r8 g8 af4 af
  g4. r8 r2

  R1*4
  r4 r8 af8 g4 g
  af4. r8 r2
  r4 r8 af8 g4 g
  af4. r8 r2
  R1*3
  r2

  af8 ef f g
  af2. bf4
  bf2. bf8 c
  df2. ef4
  c2. \breathe c4
  df2. 4
  ef2. df4 c2 c
  <c f>1
}

alto = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*13
  r4 r8 f8 ef4 ef
  f4. r8 r2
  r4 r8 ef8 f4 f
  ef4. r8 r2

  R1*4
  r4 r8 f8 ef4 ef
  f4. r8 r2
  r4 r8 f8 ef4 ef
  f4. r8 r2
  R1*3
  r2 r8

  ef8 f g
  af2 af4 bf
  g2 ~ 8 g f g
  af4 af2 af8 bf
  g2 c4 bf
  c4 c2.
  r2 f,4 f
  f4 f2.

  R1
  r4 r8 f8 ef4 ef
  f4. r8 r2
  r4 r8 ef8 f4 f
  ef4. r8 r2

  R1*4
  r4 r8 f8 ef4 ef
  f4. r8 r2
  r4 r8 f8 ef4 ef
  f4. r8 r2
  R1*3
  r2

  af8 ef f g
  af2. bf4
  g2. g8 af
  bf2. c4
  af2. \breathe af4
  af2. bf4
  c2. af4 g2 g
  a1
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8
  \set Staff.shortInstrumentName = #"T."
  R1*5
  \set Staff.shortInstrumentName = #""
  r4 \once\override DynamicText.self-alignment-X = #RIGHT r8\p c8 c4 bf
  c4. r8 r2
  r4 r8 c8 c4 c
  c4. r8 r2

  R1*4
  r4 \once\override DynamicText.self-alignment-X = #RIGHT r8\mp c8 c4 bf
  c4. r8 r2
  r4 r8 c8 c4 bf
  c4. r8 r2
  R1*3
  r2

  \set crescendoText = \markup { \italic { cresc. poco a poco} }
  \set crescendoSpanner = #'text
  r8\< c8 df ef
  c4( ef2) df8 c
  bf4. ef8 ef4. df8
  c4 ef2 ef8 f
  ef2 c4\f bf
  c4 c2.
  r2 c4\p c
  c4 c2.

  R1
  r4 \once\override DynamicText.self-alignment-X = #RIGHT r8\mp c8 c4 bf
  c4. r8 r2
  r4 r8 c8 c4 c
  c4. r8 r2

  R1*4
  r4 r8 c8 c4 bf
  c4. r8 r2
  r4 r8 c8 c4 bf
  c4. r8 r2
  R1*3
  r2

  af8\< c df bf
  c4( ef2) df8( c)
  bf2 bf4 ef
  f2. ef4
  f2. \breathe f4
  f2. f4
  ef2. ef4 f2 e
  f1\ff
}

bass = \relative c {
  \global\dynamicUp
  % Music follows here.
  R1*4
  r4^\markup{ \smcp { bass solo } or \smcp { group } } \once\override DynamicText.self-alignment-X = #RIGHT r8\mf f8 f4 c
  ef8( f8 ~ 2.)
  r4 r8 f8 f4 f
  ef8( c8 ~ 2.)
  \set Staff.shortInstrumentName = #"B."
  r4 r8 c8 ef4 f
  af4 g8 af g f4 ef8
  f4 c2 c4
  f4. f8 ef4 bf
  c4. c8^\markup{ \smcp { solo } or \smcp { tutti } } ef4 f
  af8( f8 ~ 2.)
  \set Staff.shortInstrumentName = #""
  r4 r8 f8 f4 f
  ef8( c8 ~ 2.)
  r4 r8 c8 ef4 f
  af4 g8 af g f4 ef8
  f4 c2 c4
  f4. f8 ef4 bf
  c4. c8 ef4 f
  af8( f8 ~ 2.)
  r4 r8 c8 ef4 f
  af8( f8 ~ 2.)
  R1*4

  \set crescendoText = \markup { \italic { cresc. poco a poco} }
  \set crescendoSpanner = #'text
  r2^\markup{ \halign #0.25 \smcp { tutti } } c4\mp\< bf
  af( bf) c df
  ef ef bf ef
  af,4 bf c df
  ef2 c4\f bf
  c4 c2.
  r2 f4\p c
  f4 f2.

  r4 r8 f8\mf f4 c
  ef8( f8 ~ 2.)
  r4 r8 f8 f4 f
  ef8( c8 ~ 2.)
  r4 r8 c8 ef4 f
  af4 g8 af g f4 ef8
  f4 c2 c4
  f4. f8 ef4 bf
  c4. c8 ef4 f
  af8( f8 ~ 2.)
  r4 r8 c8 ef4 f
  af8( f8 ~ 2.)
  R1*4

  r2 c8\mp\< df c bf
  af4( bf c) df
  ef2. df8 c
  bf4( c df) ef
  f2. \breathe ef4
  df2. bf4
  af2. af8( bf) c2 c
  <f f,>1\ff
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  And by the help of your grace,
  may they be wor -- thy to es -- cape
  con -- dem -- na -- tion,
  con -- dem -- na -- tion.

  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  So they may en -- joy the bliss
  of e -- ter -- nal light,
  e -- ter -- nal light,
  e -- ter -- nal light.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  And by the help of your grace,
  may they be wor -- thy to es -- cape
  con -- dem -- na -- tion,
  con -- dem -- na -- tion.

  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  So they may en -- joy the bliss
  of e -- ter -- nal light,
  e -- ter -- nal light,
  e -- ter -- nal light.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  And by the help of your grace,
  may they be wor -- thy to es -- cape
  con -- dem -- na -- tion,
  con -- dem -- na -- tion.

  Ab -- solve, O Lord,
  ab -- solve, O Lord.
  Ab -- solve, O Lord,
  ab -- solve, O Lord.

  So they may en -- joy the bliss
  of e -- ter -- nal light,
  e -- ter -- nal light,
  e -- ter -- nal light.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  Ab -- solve, O Lord,
  ab -- solve, O Lord,
  ab -- solve the souls of the faith -- ful de -- par -- ted
  from e -- v’ry bond of sin.
  Ab -- solve, O Lord,
  ab -- solve, O Lord,
  ab -- solve the souls of the faith -- ful de -- par -- ted
  from e -- v’ry bond of sin.
  Ab -- solve, O Lord,
  ab -- solve them, Lord.

  By the help of your grace,
  may they be wor -- thy to es -- cape
  con -- dem -- na -- tion,
  con -- dem -- na -- tion.

  Ab -- solve, O Lord,
  ab -- solve, O Lord,
  ab -- solve the souls of the faith -- ful de -- par -- ted
  from e -- v’ry bond of sin.
  Ab -- solve, O Lord,
  ab -- solve them, Lord,

  so they may en -- joy the bliss
  of e -- ter -- nal light,
  e -- ter -- nal light,
  e -- ter -- nal light.
}

right = \relative c' {
  \global
  r4 r8 f8 f4 c
  ef8( f8 ~ 2.)
  r4 r8 c8 ef4 f
  af8( f8 ~ 2.)
  R1*20

  r4 r8 f8 f4 c
  ef8( f4) <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4. c,8 ef4 f
  af8( f4) <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4 <bf f df> <af ef c>8 <ef c>8 <f df>8 <g ef>
  <af ef c>2 <af f ef>4 << { \voiceOne <bf f>4 } \new Voice { \voiceTwo df,8 c } >> \oneVoice
  <g' bf,>1
  <c af ef>1
  <bf g ef>2 c4 bf
  c4. <af f c>8 <g ef c>4 q4
  <af f c>4. r8 r2
  r4 r8 <af f c>8 <af g ef>4 q4
  <af f c>4. r8 r2
  r4 r8 <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4. r8 r2
  r4 r8 <g ef c>8 <af f c>4 q4
  <g ef c>4. r8 r2
  f8 g af bf c4. bf8
  c4 f8 ef c4 af8 bf
  c4. c8 bf8 c df ef
  c4. c8 bf4 af
  f4 r8 <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4. r8 r2
  r4 r8 <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4. f8 f4 c
  ef8( f4) <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4. c,8 ef4 f
  af8( f4) <af f c>8 <g ef c>4 <g ef bf>4
  <af f c>4 <bf f df> <af ef c>8 <ef c>8 <f df>8 <g ef>
  <af ef c>2 <af f ef>4 << { \voiceOne <bf f>4 } \new Voice { \voiceTwo df,8 c } >> \oneVoice
  <bf' g ef>1
  <df bf f>1
  <c af f>1
  <df af f>1
  <ef c af>2. <df af ef>4
  <c g f>2 <c g e>
  <f c a f>1
}

left = \relative c, {
  \global
  R1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>1
  <g c>2 <f c'>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <g c>2 <f c'>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>4 <f bf> c' bf
  af bf c df
  ef2 bf4 ef
  af,4 bf c df
  ef2 c4 bf
  c1
  <f, c'>1 ~
  q1 ~
  q1
  <f c'>2 <f bf>
  <f c'>1
  <g c>2 <f c'>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <g c>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
    <f c'>1
  <f c'>2 <f bf>
  <f c'>1
  <f c'>2 <f bf>
  <f c'>4 bf << { \voiceTwo s4 ef, } \new Voice { \voiceOne c'8 df c bf } >> \oneVoice
  af4 bf c df
  <ef ef,>2. df8 c
  bf4( c df) ef
  <f f,>2. \breathe ef4
  df2. bf4
  af2. af8( bf) c2 c
  <f, c'>1
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
    \new Voice { \dynamicUp \saControl }
  >>
%{
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } <<
    \new Voice = "soprano" { \soprano }
    \new Voice { \dynamicUp \saControl }
  >>
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } <<
    \new Voice = "alto" { \alto }
    \new Voice { \dynamicUp \saControl }
  >>
%}
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
