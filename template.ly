\version "2.20.0"
\language "english"
\include "common.ly"

\header {
  title = ""
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {%768
  \key c \major
  \time 2/2
  \tempo 4=120
}

tmControl = {
  s1
}

saControl = {
  s1
}

tbControl = {
 s1
}

pnControl = {
  s1
}


soprano = \relative c' {
  \global
  % Music follows here.
}

alto = \relative c' {
  \global
  % Music follows here.
}

tenor = \relative c' {
  \global
  % Music follows here.
}

bass = \relative c {
  \global
  % Music follows here.
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
}

rehearsalMidi = #
(define-music-function
 (parser location name midiInstrument lyrics) (string? string? ly:music?)
 #{
   \unfoldRepeats <<
     \new Staff = "soprano" \new Voice = "soprano" { \soprano }
     \new Staff = "alto" \new Voice = "alto" { \alto }
     \new Staff = "tenor" \new Voice = "tenor" { \tenor }
     \new Staff = "bass" \new Voice = "bass" { \bass }
     \context Staff = $name {
       \set Score.midiMinimumVolume = #0.5
       \set Score.midiMaximumVolume = #0.5
       \set Score.tempoWholesPerMinute = #(ly:make-moment 120 4)
       \set Staff.midiMinimumVolume = #0.8
       \set Staff.midiMaximumVolume = #1.0
       \set Staff.midiInstrument = $midiInstrument
     }
     \new Lyrics \with {
       alignBelowContext = $name
     } \lyricsto $name $lyrics
   >>
 #})

right = \relative c'' {
  \global
  % Music follows here.
}

left = \relative c {
  \global
  % Music follows here.
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "sa" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "S." "A." }
  } <<
    \new Voice = "soprano" { \voiceOne \soprano }
    \new Voice = "alto" { \voiceTwo \alto }
    \new Voice { \dynamicUp \saControl }
  >>
  \new Lyrics \with {
    alignAboveContext = "sa"
    \override VerticalAxisGroup #'staff-affinity = #DOWN
  } \lyricsto "soprano" \sopranoVerse
  \new Lyrics \lyricsto "alto" \altoVerse
  \new Staff = "tb" \with {
    midiInstrument = "choir aahs"
    instrumentName = \markup \center-column { "T." "B." }
  } <<
    \clef bass
    \new Voice = "tenor" { \voiceOne \tenor }
    \new Voice = "bass" { \voiceTwo \bass }
    \new Voice { \dynamicUp \tbControl }
  >>
  \new Lyrics \with {
    alignAboveContext = "tb"
    \override VerticalAxisGroup #'staff-affinity = #DOWN
  } \lyricsto "tenor" \tenorVerse
  \new Lyrics \lyricsto "bass" \bassVerse
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>

\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
  \midi { }
}

% Rehearsal MIDI files:
\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassVerse
    \midi { }
  }
}
