\version "2.20.0"
\include "05-Domine-Iesu-Christe_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
