\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key g \major
  \time 4/4
  \tempo "Brightly" 4=100
}

tmControl = {
  s1 * 8 \bar"||"\mark\default
  s1 * 21 \bar"||"\mark\default
  s1 * 17 \bar"||"\mark\default
  s1 * 18 \bar"||"\mark\default
  s1 * 21 \bar"||"\mark\default
  s1 * 15
  \override TextSpanner.bound-details.left.text = "rall."
  s4\startTextSpan s2.
  s1
  s2. s4\stopTextSpan \bar"|."
}

tempotrack = {
  s1*100
  \tempo 4=96
  s4
  \tempo 4=92
  s4
  \tempo 4=88
  s4
  \tempo 4=84
  s4
  \tempo 4=80
  s4
  \tempo 4=76
  s4
  \tempo 4=72
  s4
  \tempo 4=68
  s4
  \tempo 4=64
  s1
}

saControl = {
  s1
}

tbControl = {
  s1
}

pnControl = {
  s4\p\< s2.
  s1
  s1\mp
  s1*3
  s1\<
  s1\mf

  s1\mp\<
  s1\mf\>
  s1\!
  s1*14
  s2 s2\<
  s1\mf
  s1*2
  s4\> s2.
  s1\p\cresc
  s1*3
  s2\f s2\>
  s1\mp

  s1*7
  s1\>
  s1\p
  s1*3
  s1\<
  s1
  s1\mp
  s1
  s1\<
  s1
  s1\mf
  s2 s2\cresc
  s1*5
  s1\f
  s1*2

  s1\>
  s1
  s1\mp
  s1*14
  s2 s2\<
  s1\mf
  s1*2
  s4\> s2.
  s1\p\cresc
  s1*3
  s2\f s2\>
  s1\mf
  s1*4
  s1\>
  s1
  s1\mp
}

soprano = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8
  R1^\markup{ \smcp { sopranos } }
  r2

  \set Staff.shortInstrumentName = #"S."
  d2\mp
  b'2 a4 g
  c4. b8 a4 c
  b2 g2
  R1
  \set Staff.shortInstrumentName = #""
  b2.(\< c4)
  a2.\> r4\!
  b2.(\< d4)
  g,2\> \breathe d2\!
  d'2\mf c4 b
  e4. d8 c4 e
  d2 b
  R1
  \set Staff.shortInstrumentName = #"S."
  b2(\< c4 d4)
  a2.\> r4\!
  b2(\< c4 d4)
  g,2.\> r4\!
  \set Staff.shortInstrumentName = #""
  R1
  R1^\markup{ \smcp { sopranos } }
  R1

  r2 a4\p\cresc b
  c1 ~
  2 a4 b
  c1 ~
  4 c d c
  b1\f
  r4 g4\mp\< g a
  b4.\> g8 g2 ~
  g2 g4\< a
  b4.\> g8 g2 ~
  g2 g4\< a
  b1\!
  g2.\> g4
  g1\!
  R1*3

  R1*8
  b2\mf 4 4
  cs4 4 a g
  b1
  b2\cresc b4 a4
  b2. c4
  a2. b4
  c2. a4
  b1\f
  R1*3

  r2 a4\mp g
  b2 a4 g
  c4. b8 a4 c
  b2 g2
  R1*4
  r2 a4 g
  d'2 c4 b
  e4. d8 c4 e
  d2 b
  r2. c4
  b2 c4( d4)
  a2. c4
  b2 c4 d4
  g,2. r4
  R1
  R1^\markup{ \smcp { sopranos } }
  R1

  r2 a4\p\cresc b
  c1 ~
  2 a4 b
  c1 ~
  4 c d c
  b1\f
  r4 g4\mf g a
  b4. g8 g2 ~
  g2 g4 a
  b4. g8 g2 ~
  g2 g4 a
  b4.\> g8 g2
  a2. g4
  g1\p
  R1*4
}

alto = \relative c' {
  \global\dynamicUp
  % Music follows here.
  \set Staff.shortInstrumentName = #"A."
  R1*8

  R1*6
  \set Staff.shortInstrumentName = #""
  d1\mp\<
  e2(\> c4) r4\!
  d1\<
  e2\> \breathe d2\!
  b'2\mf a4 g
  c4. b8 a4 c
  b2 g2
  R1
  \set Staff.shortInstrumentName = #"A."
  g2(\< a4 b)
  a2.\> r4\!
  g2(\< a4 g)
  e2.\> r4\!
  \set Staff.shortInstrumentName = #""
  R1
  R1^\markup{ \smcp { altos } }
  R1

  R1
  r2 e4\p\cresc fs
  g1 ~
  2 g4 fs
  g4 g g fs
  g1\f
  r4 g4\mp g g
  f2. f4
  e2 e4 e
  ef2. ef4
  d2 d4 d
  c2.( d4)
  ef2. ef4
  d1
  R1*3

  R1*6
  e2\mp e4 e4
  g4 4 a4 4
  e1
  r2 e2\mf
  e2. fs4
  g2\cresc 2
  g2 2
  g2 fs2
  g2. e4
  g1\f
  R1*3

  r2 e4\mp e
  d2 c4 b
  e4. d8 c4 e
  d2 d
  R1*4
  r2 e4 e
  b'2 a4 g
  c4. b8 a4 c
  b2 g2
  r2. a4
  g2 a4( b)
  a2. a4
  g2 a4 g
  e2. r4
  R1
  R1^\markup{ \smcp { altos } }
  R1

  R1
  r2 e4\p\cresc fs
  g1 ~
  2 g4 fs
  g4 g g fs
  g1\f
  r4 g4\mf g g
  f2. f4
  e2 e4 e
  ef2. ef4
  d2 d4 d
  c2.(\> d4)
  ef2. ef4
  d1\p
  R1*4
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  \set Staff.shortInstrumentName = #"T."
  R1*8

  R1*14
  d1\<
  c2.\> r4\!
  b1\<
  c2.\> r4\!
  \set Staff.shortInstrumentName = #""
  R1
  R1^\markup{ \smcp { tenors } }
  R1

  R1*2
  r2 c4\mp\cresc d
  e1 ~
  4 e4 e c
  d1\f
  R1
  d2.\mp d4
  c1
  c2. c4
  b1
  c1
  c1
  b1
  R1*2
  R1^\markup{ \smcp { tenors } }

  e,2\p e4 e
  g4 4 a4 4
  e1
  R1
  b'2\mp 4 4
  cs4 4 a g
  b1
  R1
  e2\mf 4 4
  e4 4 cs4 4
  b1
  b2\cresc b4 cs4
  d2 2
  d2. 4
  e2 c
  d1\f
  R1*3

  c2\mp g2
  g1
  g2. a4
  b2 2
  g2. a4
  b1
  g1
  R1
  g2 2
  d'2 4 4
  c4. d8 e4 c
  b2 d2
  r2. c4
  d2 d2
  c2. c4
  b2 b2
  c2. r4
  R1
  R1^\markup{ \smcp { tenors } }
  R1

  R1*2
  r2 c4\mp\cresc d
  e1 ~
  4 e4 e c
  d1\f
  R1
  d2.\mf d4
  c1
  c2. c4
  b1
  c2.\> c4
  c2.\mp
  \once \override TextScript.self-alignment-X = #+1
  g4^\markup{ \right-align { opt. \smcp { solo } \hspace #2 } }\mf
  b2 a4 g
  c4. b8 a4 c
  b2 g2 ~
  g2 r2
  R1
}

bass = \relative c' {
  \global\dynamicUp
  % Music follows here.
  \set Staff.shortInstrumentName = #"B."
  R1*8

  R1*14
  g1\<
  e2.\> r4\!
  d1\<
  c2.\> r4\!
  \set Staff.shortInstrumentName = #""
  R1*3

  R1*3
  r2 e4\mf\cresc d
  c4 c c c
  g1\f
  R1
  g2.\mp g4
  g1
  g2. g4
  g2 g'4 fs
  e2.( d4)
  c2. c4
  g1
  R1*2
  R1^\markup{ \smcp { basses } }

  R1*2
  e'2\p e4 e4
  g4 4 a4 4
  e1
  R1
  e2\mp 4 4
  a,4 a4 cs4 d4
  e1
  r2 e2\mf
  e1\cresc
  e1
  g2 2
  d2 2
  c2 d2
  g1\f
  R1*3

  c,2\mp 2
  g1
  c2. c4
  g2 2
  c2. c4
  g1
  c1
  R1
  c2 2
  g'1
  g2. 4
  2 2
  2. 4
  g2 2
  e2. e4
  d2 2
  c2. r4

  R1*6
  r2 e4\mf\cresc d
  c4 c c c
  g1\f
  R1
  g2.\mf g4
  g1
  g2. g4
  g2 g'4 fs
  e2.(\> d4)
  c2. c4
  g1\p
  R1*4
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  May light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,
  may light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,

  with your Saints, __ with your Saints __ for e -- ver -- more:

  for you are mer -- ci -- ful, __
  you are mer -- ci -- ful, __
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tu -- al light,

  with your Saints, __ with your Saints __ for e -- ver -- more:

  for you are mer -- ci -- ful, __
  you are mer -- ci -- ful, __
  you are mer -- ci -- ful,
  mer -- ci -- ful.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,
  may light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,

  with your Saints, __ with your Saints for e -- ver -- more:

  for you are mer -- ci -- ful,
  you are mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest.
  Grant them e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tu -- al light,

  with your Saints, __ with your Saints for e -- ver -- more:

  for you are mer -- ci -- ful,
  you are mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,

  with your Saints __ for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tual light,

  with your Saints __ for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.
  May light shine per -- pe -- tual -- ly up -- on them.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,

  with your Saints for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful, __
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  E -- ter -- nal rest, grant them e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly,
  e -- ter -- nal light,
  per -- pe -- tual light,

  with your Saints for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful, __
  mer -- ci -- ful.
}

right = \relative c'' {
  \global
  R1
  r4 d g a
  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <c e> a <b d> <a c>
  <g b>1
  <e d'>1^\tenuto \breathe

  <g b>2
  << { \voiceTwo d2 e4. e8 g } \new Voice { \voiceOne a4 g d'2 ~ 8 } >> \oneVoice
  d8 g a
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c'8 d, g d e g c d
  b8 d, g d r d g d
  a'8 c, e c r c g' c,
  b'8 d, g d r d g d
  c'8 e, g e r d g a
  <b d>8 d, g d r d <g b> d
  <c' e>8 e, g e r e <g c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d e g c d
  b8 d, g d r d g d
  a'8 c, e c r c g' c,
  b'8 d, g d r d g d
  c'8 d, g d r d g a

  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <e c> g a b
  <d, g a>1 ~
  1
  <e g a> ~
  1
  <g b>2 g8 a b d
  g d b' d, g d a' d,
  b' d, f d r g f d
  e g, c g r e' g, c
  ef g, c g r ef' g, c
  d g, b g r g b d
  e b c g r b c b
  ef b c g r ef' f ef
  d4 b a g
  c4. b8 a4 g

  e2 e4 fs
  g4 a fs d
  e1
  g2 a
  e2 g2 ~
  g2 a
  << { \voiceTwo
    e1 ~
    e1 ~
    e1 ~
    e1 ~
    e2 g2 ~
    4 fs e2 ~
    e2. fs4
    g1 ~
    1 ~
    <g d>2 ~ <fs d>2
    e2 c
    d4. 8
  } \new Voice { \voiceOne
    b'1
    cs2 a4 g
    b1
    g2 a
    b1
    cs2 a4 g
    b1
    b2. a4
    b2. c4
    a2. b4
    <c g>2. <a e>4
    <b g>2
  } >> \oneVoice

  g8 d' g a
  << { \voiceOne
    b8 d, g d r d g d
    c'8 e, g e r e g e
    b'8 d, g d r d g d
  } \new Voice { \voiceTwo
    b2 a4 g
    c4. b8 a4 c
    b2 g2
  } >> \oneVoice
  c'8 e, g e <d a> e g a
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c'8 d, g d e g c d
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c' e, g e <d a> e g a
  <b d>8 d, g d r d <g b> d
  <c' e>8 e, g e r e <g c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d e g c d
  <g, b>8 d g d r d <g b> d
  <e a>8 c e c r e <a c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d r d g a

  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <e c> g a b
  <d, g a>1
  <d g a>
  <e g a>
  <e g a>
  <g b>2 g8 a b d
  g d b' d, g d a' d,
  b' d, f d r g f d
  e g, c g r e' g, c
  ef g, c g r ef' g, c
  d g, b g r g b d
  e b c g r b c b
  ef a, c g r ef' f ef
  << { \voiceOne
   <d b>2 <a c>4 <b g>4
  } \new Voice { \voiceTwo
   r8 d,8 r d r d r d
  } >> \oneVoice
  <e' c>4. <d b>8 <c a>4 <e c>4
  << { \voiceOne
    <d b>4 ~ d b8 d g a
    b8 d, g d r d g d
    %b'8 d, g d r d g d
    <g b>1
  } \new Voice { \voiceTwo
    d,8 g b g b2
    b1 ~
    %1 ~
    1
  } >> \oneVoice

}

left = \relative c {
  \global
  g1 ~
  << { \voiceTwo
    1 ~
    2 ~ 2 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1\tenuto
  } \new Voice { \voiceOne
    s1
    s2 a2
    b4 c d2
    e2 d2
    c2 b4 c
    d1
    c
  } >> \oneVoice

  \repeat unfold 9 {
    <g d'>1 ~
    <g c>1
  }

  << { \voiceTwo
    g1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    d'2 a2
    b4 c d2
    e2 d2
    c1
    r8 c d c r c d c
    r8 c d c r c d c
    r8 c e c r c d c
    r8 c e c r c d c
  } >> \oneVoice
  <g d'>1 ~
  <g d'>1
  <g d'>1 ~
  <g c>1
  <g c>1 ~
  <g d'>1
  <g c>1 ~
  <g c>1
  <g d'>1 ~
  <g c>1

  << { \voiceTwo
    g2 e2 ~
    e1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    g1
    s1
  } \new Voice { \voiceOne
    c1
    b1
    b1
    a2 cs
    b1
    a2 cs
    b1
    a1
    b1
    a2 cs4 d
    <b e>1
    a1
    b1 ~
    b2. cs4
    d1 ~
    1
  } >> \oneVoice
  c2 d

  << { \voiceTwo
    g,1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    r2 d'2
    d2 c4 b
    e4. d8 c4 e
    d2 d
    c1
  } >> \oneVoice
  \repeat unfold 8 {
    <g d'>1 ~
    <g c>1
  }

  << { \voiceTwo
    g1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    d'2 a2
    b4 c d2
    e2 d2
    c1
    r8 c d c r c d c
    r8 c d c r c d c
    r8 c e c r c d c
    r8 c e c r c d c
  } >> \oneVoice
  <g d'>1 ~
  <g d'>1
  <g d'>1 ~
  <g c>1
  <g c>1 ~
  <g d'>1
  <g c>1 ~
  <g c>1
  <g d'>1 ~
  <g c>1
  <g d'>1 ~
  1 ~
  %1 ~
  1
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
