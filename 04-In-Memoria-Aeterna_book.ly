\version "2.20.0"
\include "04-In-Memoria-Aeterna_music.ly"

\paper {
  system-system-spacing.padding = #4
}

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}

\markuplist{
  \override-lines #'(line-width . 103)
  \justified-lines {
The repeat in this movement should only be performed if a different, and ideally
fuller, voicing is used the second time. For example, the first time could be
solo soprano and solo alto, and the second time a group of sopranos and altos.
  }
}
