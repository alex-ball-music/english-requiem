\version "2.20.0"

\layout {
  ragged-last = ##t
  \context {
    \Score
    timing = ##f
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 3 16)
  }
  \context {
    \Staff
    \hide Stem
    \override Stem #'length = #0
  }
}
