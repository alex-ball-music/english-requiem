global = {
  \key g \major
  \time 4/4
}

pnControl = {
  s1
}

soprano = \relative c'' {
  \global
  % Music follows here.
  r2 a
  c4 b2 c4
  a1 \bar"|."
}

alto = \relative c' {
  \global
  % Music follows here.
  r2 e2
  f2 g4 g
  fs1
}

tenor = \relative c' {
  \global
  % Music follows here.
  r2 c2
  c4 d2 c4
  d1
}

bass = \relative c {
  \global
  % Music follows here.
  r2 a'4( g)
  f4 f2 e4
  d1
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  And al -- so with you.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  And al -- so with you.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  And al -- so with you.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  And al -- so with you.
}

right = \relative c'' {
  \global
  << { \voiceOne
    r2 a
    c4 b2 c4
    a1 \bar"||"
  } \new Voice { \voiceTwo
    r2 e2
    f2 g4 g
    fs1
  } >>
}

left = \relative c' {
  \global
  << { \voiceOne
    r2 c2
    c4 d2 c4
    d1
  } \new Voice { \voiceTwo
    a,2 a'4( g)
    f4 f2 e4
    d1
  } >> \oneVoice
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
