\version "2.20.0"
\language "english"

\header {
  title = "Sandpit"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  ragged-last = ##t
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key g \major
  \time 2/2
  \tempo 4=100
}

soprano = \relative c' {
  \global
  % Music follows here.
  R1*9
  r2

  d2
  b'2 a4 g
  c4. b8 a4 c
  b2 g2
  R1
  b2.( c4)
  a2. r4
  b2.( d4)
  g,2 \breathe d2
  d'2 c4 b
  e4. d8 c4 e
  d2 b
  R1
  b2( c4 d4)
  a2. r4
  b2( c4 d4)
  g,2. r4

  R1*3
  r2 a4 b
  c1 ~
  2 a4 b
  c1 ~
  4 c d c
  b1
  r4 g4 g a
  b4. g8 g2 ~
  g2 g4 a
  b4. g8 g2 ~
  g2 g4 a
  b1
  g2. g4
  g1
  R1*3

  R1*8
  b2 4 4
  cs4 4 a g
  b1
  b2 b4 a4
  b2. c4
  a2. b4
  c2. a4
  b1
  R1*3

  r2 a4 g
  b2 a4 g
  c4. b8 a4 c
  b2 g2
  R1*4
  r2 a4 g
  d'2 c4 b
  e4. d8 c4 e
  d2 b
  r2. c4
  b2 c4( d4)
  a2. c4
  b2 c4 d4
  g,2. r4

  R1*3
  r2 a4 b
  c1 ~
  2 a4 b
  c1 ~
  4 c d c
  b1
  r4 g4 g a
  b4. g8 g2 ~
  g2 g4 a
  b4. g8 g2 ~
  g2 g4 a
  b4. g8 g2
  a2. g4
  g1
  R1*5
}

alto = \relative c' {
  \global
  % Music follows here.
  R1*10

  R1*4
  d1
  e2( c4) r4
  d1
  e2 \breathe d2
  b'2 a4 g
  c4. b8 a4 c
  b2 g2
  R1
  g2( a4 b)
  a2. r4
  g2( a4 g)
  e2. r4

  R1*4
  r2 e4 fs
  g1 ~
  2 g4 fs
  g4 g g fs
  g1
  r4 g4 g g
  f2. f4
  e2 e4 e
  ef2. ef4
  d2 d4 d
  c2.( d4)
  ef2. ef4
  d1
  R1*3

  R1*6
  e2 e4 e4
  g4 4 a4 4
  e1
  r2 e2
  e2. fs4
  g2 2
  g2 2
  g2 fs2
  g2. e4
  g1
  R1*3

  r2 e4 e
  d2 c4 b
  e4. d8 c4 e
  d2 d
  R1*4
  r2 e4 e
  b'2 a4 g
  c4. b8 a4 c
  b2 g2
  r2. a4
  g2 a4( b)
  a2. a4
  g2 a4 g
  e2. r4

  R1*4
  r2 e4 fs
  g1 ~
  2 g4 fs
  g4 g g fs
  g1
  r4 g4 g g
  f2. f4
  e2 e4 e
  ef2. ef4
  d2 d4 d
  c2.( d4)
  ef2. ef4
  d1
  R1*5
}

tenor = \relative c' {
  \global
  % Music follows here.
  R1*10

  R1*12
  d1
  c2. r4
  b1
  c2. r4

  R1*5
  r2 c4 d
  e1 ~
  4 e4 e c
  d1
  R1
  d2. d4
  c1
  c2. c4
  b1
  c1
  c1
  b1
  R1*3

  e,2 e4 e
  g4 4 a4 4
  e1
  R1
  b'2 4 4
  cs4 4 a g
  b1
  R1
  e2 4 4
  e4 4 cs4 4
  b1
  b2 b4 cs4
  d2 2
  d2. 4
  e2 c
  d1
  R1*3

  c2 g2
  g1
  g2. a4
  b2 2
  g2. a4
  b1
  g1
  R1
  g2 2
  d'2 4 4
  c4. d8 e4 c
  b2 d2
  r2. c4
  d2 d2
  c2. c4
  b2 b2
  c2. r4

  R1*5
  r2 c4 d
  e1 ~
  4 e4 e c
  d1
  R1
  d2. d4
  c1
  c2. c4
  b1
  c2. c4
  c2. g4
  b2 a4 g
  c4. b8 a4 c
  b2 g2 ~
  g1
  R1*2
}

bass = \relative c' {
  \global
  % Music follows here.
  R1*10

  R1*12
  g1
  e2. r4
  d1
  c2. r4

  R1*6
  r2 e4 d
  c4 c c c
  g1
  R1
  g2. g4
  g1
  g2. g4
  g2 g'4 fs
  e2.( d4)
  c2. c4
  g1
  R1*3

  R1*2
  e'2 e4 e4
  g4 4 a4 4
  e1
  R1
  e2 4 4
  a,4 a4 cs4 d4
  e1
  r2 e2
  e1
  e1
  g2 2
  d2 2
  c2 d2
  g1
  R1*3

  c,2 2
  g1
  c2. c4
  g2 2
  c2. c4
  g1
  c1
  R1
  c2 2
  g'1
  g2. 4
  2 2
  2. 4
  g2 2
  e2. e4
  d2 2
  c2. r4

  R1*6
  r2 e4 d
  c4 c c c
  g1
  R1
  g2. g4
  g1
  g2. g4
  g2 g'4 fs
  e2.( d4)
  c2. c4
  g1
  R1*5
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  May light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,
  may light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,

  with your Saints, __ with your Saints __ for e -- ver -- more:

  for you are mer -- ci -- ful, __
  you are mer -- ci -- ful, __
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tu -- al light,

  with your Saints, __ with your Saints __ for e -- ver -- more:

  for you are mer -- ci -- ful, __
  you are mer -- ci -- ful, __
  you are mer -- ci -- ful,
  mer -- ci -- ful.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,
  may light shine e -- ter -- nal -- ly up -- on them.
  O __ Lord, O __ Lord,

  with your Saints, __ with your Saints for e -- ver -- more:

  for you are mer -- ci -- ful,
  you are mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest.
  Grant them e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tu -- al light,

  with your Saints, __ with your Saints for e -- ver -- more:

  for you are mer -- ci -- ful,
  you are mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,

  with your Saints __ for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly up -- on them,
  e -- ter -- nal light,
  per -- pe -- tual light,

  with your Saints __ for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful.
  May light shine per -- pe -- tual -- ly up -- on them.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  O Lord, O Lord,

  with your Saints for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful, __
  mer -- ci -- ful.

  Grant them e -- ter -- nal rest, O Lord.
  Grant them e -- ter -- nal rest, O Lord.
  E -- ter -- nal rest, grant them e -- ter -- nal rest.

  And may light shine per -- pe -- tual -- ly up -- on them.
  And may light shine per -- pe -- tual -- ly,
  e -- ter -- nal light,
  per -- pe -- tual light,

  with your Saints for e -- ver -- more:

  mer -- ci -- ful,
  mer -- ci -- ful,
  mer -- ci -- ful, __
  mer -- ci -- ful.
}

right = \relative c'' {
  \global
  R1
  r4 d g a
  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <c e> a <b d> <a c>
%{  <b d>8 d, g d <a' c> d, g d
  <g b> d g d <fs a> d e d
  <e g> b c b <d fs> a b a
  <c e> g a g <b d> g a fs
%}  <g b>1
  <e d'>1
  <g b>2 << { \voiceOne a4 g } \new Voice { \voiceTwo d2 } >> \oneVoice
  <e d'>2 ~ <g d'>8 d' g a
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c'8 d, g d e g c d
  b8 d, g d r d g d
  a'8 c, e c r c g' c,
  b'8 d, g d r d g d
  c'8 e, g e r d g a
  <b d>8 d, g d r d <g b> d
  <c' e>8 e, g e r e <g c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d e g c d
  b8 d, g d r d g d
  a'8 c, e c r c g' c,
  b'8 d, g d r d g d
  c'8 d, g d r d g a
  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <e c> g a b
  <d, g a>1 ~
  1
  <e g a> ~
  1
  <g b>2 g8 a b d
  g d b' d, g d a' d,
  b' d, f d r g f d
  e g, c g r e' g, c
  ef g, c g r ef' g, c
  d g, b g r g b d
  e b c g r b c b
  ef b c g r ef' f ef
  d4 b a g
  c4. b8 a4 g
  e2 e4 fs
  g4 a fs d
  e1
  g2 a
  e2 g2 ~
  g2 a
  << { \voiceTwo
    e1 ~
    e1 ~
    e1 ~
    e1 ~
    e2 g2 ~
    4 fs e2 ~
    e2. fs4
    g1 ~
    1 ~
    <g d>2 ~ <fs d>2
    e2 c
    d2
  } \new Voice { \voiceOne
    b'1
    cs2 a4 g
    b1
    g2 a
    b1
    cs2 a4 g
    b1
    b2. a4
    b2. c4
    a2. b4
    <c g>2. <a e>4
    <b g>2
  } >> \oneVoice
  g8 d' g a
  << { \voiceOne
    b8 d, g d r d g d
    c'8 e, g e r e g e
    b'8 d, g d r d g d
  } \new Voice { \voiceTwo
    b2 a4 g
    c4. b8 a4 c
    b2 g2
  } >> \oneVoice
  c'8 e, g e <d a> e g a
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c'8 d, g d e g c d
  b8 d, g d r d g d
  c'8 d, g d r d g d
  b'8 d, g d r d g d
  c' e, g e <d a> e g a
  <b d>8 d, g d r d <g b> d
  <c' e>8 e, g e r e <g c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d e g c d
  <g, b>8 d g d r d <g b> d
  <e a>8 c e c r e <a c> e
  <b' d>8 d, g d r d <g b> d
  <g c>8 d g d r d g a
  <b d>4 g <a c> fs
  <g b> e <fs a> d
  <e g> c <d fs> a
  <e c> g a b
  <d, g a>1
  <d g a>
  <e g a>
  <e g a>
  <g b>2 g8 a b d
  g d b' d, g d a' d,
  b' d, f d r g f d
  e g, c g r e' g, c
  ef g, c g r ef' g, c
  d g, b g r g b d
  e b c g r b c b
  ef a, c g r ef' f ef
  << { \voiceOne <d b>2 } \new Voice { \voiceTwo r8 d,8 r d } >> \oneVoice
  <a' c>8 d, <b' g> d,
  <e' c>4. <d b>8 <c a>4 <e c>4
  << { \voiceOne
    <d b>4 ~ d b8 d g a
    b8 d, g d r d g d
    b'8 d, g d r d g d
    <g b>1
  } \new Voice { \voiceTwo
    d,8 g b g b2
    b1 ~
    1 ~
    1
  } >> \oneVoice

}

left = \relative c {
  \global
  g1 ~
  << { \voiceTwo
    1 ~
    2 ~ 2 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    s1
    s2 a2
    b4 c d2
    e2 d2
    c2 b4 c
    d1
    c
  } >> \oneVoice
  \repeat unfold 9 {
    <g d'>1 ~
    <g c>1
  }
  << { \voiceTwo
    g1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    d'2 a2
    b4 c d2
    e2 d2
    c1
    r8 c d c r c d c
    r8 c d c r c d c
    r8 c e c r c d c
    r8 c e c r c d c
  } >> \oneVoice
  <g d'>1 ~
  <g d'>1
  <g d'>1 ~
  <g c>1
  <g c>1 ~
  <g d'>1
  <g c>1 ~
  <g c>1
  <g d'>1 ~
  <g c>1
  << { \voiceTwo
    g2 e2 ~
    e1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    1 ~
    1
    g1
    s1
  } \new Voice { \voiceOne
    c1
    b1
    b1
    a2 cs
    b1
    a2 cs
    b1
    a1
    b1
    a2 cs4 d
    <b e>1
    a1
    b1 ~
    b2. cs4
    d1 ~
    1
  } >> \oneVoice
  c2 d
  << { \voiceTwo
    g,1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    r2 d'2
    d2 c4 b
    e4. d8 c4 e
    d2 d
    c1
  } >> \oneVoice
  \repeat unfold 8 {
    <g d'>1 ~
    <g c>1
  }
  << { \voiceTwo
    g1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
  } \new Voice { \voiceOne
    d'2 a2
    b4 c d2
    e2 d2
    c1
    r8 c d c r c d c
    r8 c d c r c d c
    r8 c e c r c d c
    r8 c e c r c d c
  } >> \oneVoice
  <g d'>1 ~
  <g d'>1
  <g d'>1 ~
  <g c>1
  <g c>1 ~
  <g d'>1
  <g c>1 ~
  <g c>1
  <g d'>1 ~
  <g c>1
  <g d'>1 ~
  1 ~
  1 ~
  1
}

\score {
  <<
    \new ChoirStaff <<
      \new Staff \with {
        midiInstrument = "choir aahs"
        instrumentName = "S."
      } { \soprano }
      \addlyrics { \sopranoVerse }
      \new Staff \with {
        midiInstrument = "choir aahs"
        instrumentName = "A."
      } { \alto }
      \addlyrics { \altoVerse }
      \new Staff \with {
        midiInstrument = "choir aahs"
        instrumentName = "T."
      } { \clef "treble_8" \tenor }
      \addlyrics { \tenorVerse }
      \new Staff \with {
        midiInstrument = "choir aahs"
        instrumentName = "B."
      } { \clef bass \bass }
      \addlyrics { \bassVerse }
    >>
    \new PianoStaff \with {
      instrumentName = "Org."
    } <<
      \new Staff = "right" \with {
        midiInstrument = "church organ"
      } \right
      \new Staff = "left" \with {
        midiInstrument = "church organ"
      } { \clef bass \left }
    >>
  >>
  \layout { }
  \midi { }
}
