# Texts to set

## Introit: Requiem Aeternam / Te Decet

Grant them eternal rest, O Lord:\
and may light shine perpetually upon them.

Praise befits you, O God, in Zion,\
our promises to you will be fulfilled:\
To you who listen to prayer,\
to you all mortals shall come.

Grant them eternal rest, O Lord:\
and may light shine perpetually upon them.


## Kyrie Eleison

*Text of ‘Kyrie Confession – Spirit’ © The Archbishops' Council 2000*

You raise the dead to life in the Spirit.\
You bring pardon and peace to the broken in heart.\
You make one by your Spirit the torn and divided.

Lord, have mercy.\
Christ, have mercy.\
Lord, have mercy.


## Gradual: Absolve, Domine

Absolve, O Lord,\
the souls of all the faithful departed:\
from every bond of sin.

And by the help of your grace,\
may they be worthy to escape condemnation:\
and enjoy the bliss of eternal light.


## Commemoration: In Memoria Aeterna

*Verses from Psalm 112 and Wisdom 3*

Light shines in the darkness for the upright;\
for they will never be shaken.

The righteous will be held in eternal remembrance;\
their head will be exalted for ever.

The souls of the righteous are in the hand of God.

No torment will ever touch them.

They seem to have died but they are at peace.


## Offertory: Domine Iesu Christe

Lord Jesus Christ, King of glory,

deliver the souls of all the faithful departed\
  from the pains of hell\
  and from the bottomless pit:\
deliver them from the lion's mouth,\
  so hell may not swallow them up,\
  nor may they fall into darkness.

Sacrifice and prayer we offer you, O Lord:\
  accept them for those departed souls\
  of whom we make memorial this day.\
Grant them, O Lord, to pass from death\
  to the life which you promised of old
  to Abraham and his children.


## Sanctus and Benedictus

Holy, holy, holy Lord,\
God of power and might,\
heaven and earth are full of your glory.\
Hosanna in the highest.

Blessed is he who comes in the name of the Lord.\
Hosanna in the highest.


## Acclamation

Praise to you, Lord Jesus:\
dying you destroyed our death,\
rising you restored our life:\
Lord Jesus, come in glory.


## Agnus Dei

O Lamb of God, you take away the sin of the world,\
grant them rest.

O Lamb of God, you take away the sin of the world,\
grant them rest.

O Lamb of God, you take away the sin of the world,\
grant them everlasting rest.


## Communion: Lux Aeterna

May light shine eternally upon them, O Lord,\
with your Saints for evermore:\
for you are merciful.

Grant them eternal rest, O Lord:\

And may light shine perpetually upon them.\
with your Saints for evermore:\
for you are merciful.
