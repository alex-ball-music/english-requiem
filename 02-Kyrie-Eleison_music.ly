\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key d \minor
  \time 2/2
  \tempo "Con anima" 2=50
}

tmControl = {
  s1*8 \bar"||"\mark\default
  s1*16 \bar"||"\mark\default
  s1*24 \bar"||"\mark\default
  s1*12
  \override TextSpanner.bound-details.left.text = "rall."
  s1\startTextSpan
  s1
  s2. s4\stopTextSpan
  s1 \bar"|."
}

tempotrack = {
  s1*60
  \tempo 2=48
  s2
  \tempo 2=46
  s2
  \tempo 2=44
  s2
  \tempo 2=42
  s2
  \tempo 2=40
  s2
  \tempo 2=38
  s2
  \tempo 2=36
  s1
}

pnControl = {
  s1\mf
  s1*7
  s2\> s2\mp
  s1*7
  s1*31
  s1\<
  s1\mf
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1*7
  R1^\markup{ \smcp { sopranos } }

  \set Staff.shortInstrumentName = #"S."
  \once\override DynamicText.self-alignment-X = #RIGHT
  a4\mf( d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d1
  R1

  \set Staff.shortInstrumentName = #""
  a'4( d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d1
  R1

  r4 a'4\f d c
  d2. e4
  f2 d4 c
  d4 a4 ~ a r
  r2 d(
  c) a
  bf2( f4 g)
  a2 r

  r2 d4 c
  a4 a4 ~ 4 g
  a2 d4 e
  f e d( c)
  a2 r
  d2 c
  bf2( f4 g)
  a2
     cs4 a
  d2 a4 d
  e4 a,2 e'4
  f2 d4 c4
  c4( d) d2
  a2 d
  e( c)
  bf2( a ~
  2) r2

  a4(\mf d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d1 ~
  2 r

  a'4( d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( bf c) d4
  e4( a,2 c4)
  <a d>1 ~
  q1
}

alto = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8
  \set Staff.shortInstrumentName = #"A."
  R1*8

  \set Staff.shortInstrumentName = #""
  r2 f4\mf e
  d4 ( c) d2
  r2 f4 e
  d4( c) d2
  r2 f4 g
  e2( d4 c)
  d1
  R1

  a'4(\mp d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d2. r4

  a'4( d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d1
  d2 f4( e)
  d4( g) a2

  a4( d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  d2. r4

  a'4(\mf d, f) g
  a4.( bf8) a2
  a2( d4) c
  a4.( bf8) a2
  a4( d, f) g
  e2( d4 c)
  c2( d ~
  1 ~
  2) r

  d2. e4
  f2 f
  f4( g a) a4
  f2 f
  a4( d, f) g
  e2( d4 c)
  d1 ~
  1
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*8
  \set Staff.shortInstrumentName = #"T."
  R1*8

  \set Staff.shortInstrumentName = #""
  r4 a4(\mf d) c
  a4.( g8) a2
  r4 a2 a4
  c2( \tuplet 3/2 { bf4 c bf) }
  a2 r
  c2 a
  bf( d ~
  d2) cs

  r4 a4(\mp d) c
  a4.( g8) a2
  r4 a2 a4
  c2( \tuplet 3/2 { bf4 c bf) }
  a2 r
  c2 a
  bf( d ~
  d2) cs

  r4 a4( d) c
  a4.( g8) a2
  r4 a2 a4
  c2( \tuplet 3/2 { bf4 c bf) }
  a2 r
  c2 a
  bf( d ~
  d2) cs

  r4 a4( d) c
  a4.( g8) a2
  r4 a2 a4
  c2( \tuplet 3/2 { bf4 c bf) }
  a2 r
  c2 a
  bf( d ~
  d2) cs

  f2\mf e
  d2 d4( e)
  f2 e
  d2 c
  a2 g
  e( f)
  f1 ~
  2 r

  f4( g a) bf
  c( d) e( d)
  c( bf a) g
  a4.( bf8) c4( d)
  d2 d2
  c2( bf4 a)
  a1 ~
  1
}

bass = \relative c {
  \global\dynamicUp
  % Music follows here.
  R1*8
  \set Staff.shortInstrumentName = #"B."
  R1*8

  \set Staff.shortInstrumentName = #""
  d2\mf d
  e e
  f f
  g g
  d d
  e e
  f f
  g a

  d,2\mp d
  e e
  f f
  g g
  d d
  e e
  f f
  g a

  d,2 d
  e e
  f f
  g g
  d d
  e e
  f f
  g a

  d,2 d
  e e
  f f
  g g
  d d
  e e
  f f
  g a

  d,2\mf e
  f g
  d e
  f e
  d g,
  c2( a)
  bf( c
  \stemUp d) \stemNeutral r

  d2( bf4) a
  d2 d2
  d2( f4) e
  d2 d2
  c2 bf
  a1
  d1 ~
  1
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

%%% Text of ‘Kyrie Confession – Spirit’ © The Archbishops' Council 2000

  You raise the dead to life in the Spi -- rit. __
  Lord, have mer -- cy.

  You bring par -- don and peace to the bro -- ken in __ heart.
  Christ, have mer -- cy.

  You make one by your Spi -- rit the torn and di -- vi -- ded.
  Lord, have mer -- cy. __

%%% End of text of ‘Kyrie Confession – Spirit’

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __

}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.
  Christ, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.
  Christ, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.
  Christ, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.
  Christ, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy.
  Christ, have mer -- cy.

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __

  Lord, have mer -- cy.
  Christ, have mer -- cy.
  Lord, have mer -- cy. __
}

right = \relative c'' {
  \global
  % Music follows here.
  r4 a' f a
  r4 e8 f g f e4
  r4 d a' f
  r4 g8 a bf a g bf
  r4 a f d
  r4 g8 f e d c4
  r4 d4 a' f
  bf8 a g f g f e f

  d2 f,
  e2 f4 g
  a4 g f e
  d2 a'4. bf8
  a2 d4 c
  a2 f4 g
  a4 g f a
  bf4. d8 c4 <a e'>

  d4 a f g
  a g e a
  c a f c'
  d g, bf c
  d4 a f g
  a g e a
  bf f d f
  g e a e

  d a' d c
  d a d e
  f c d c
  d a bf c
  d a d f
  e c a c
  bf d, f g
  a c a e

  d4 a' d c
  a c d c
  a f d' e
  f e d c
  a e a c
  d a c a
  bf d bf g
  a e cs' e

  d4 f, a d
  e a, c e
  f c <a d> c
  c d a g
  a f a d
  e a, c a
  bf d, f a
  <a d>4 d, f g

  a d,8 bf' <a f>4 g
  a4. bf8 <d a>4 c
  a4. bf8 <d a>4 c
  a d,8 bf' <a f>4 g
  <a e>4 d, <f d> <g c,>
  e4 c d c
  d4 f g a
  bf d c bf

  a2 r
  R1
  R1
  R1
  r4 a' f d
  r4 g8 f e d c4
  << { \voiceOne
    d1 ~
    1
  } \new Voice { \voiceTwo
    a4 d, ~ <d f>  ~ <d g>
    <d a'>1
  } >>
}

left = \relative c {
  \global
  % Music follows here.
  d1
  e1
  f1
  g1
  d1
  e1
  f1
  g2 a

  d,1
  e1
  f1
  g1
  d1
  e1
  f1
  g2 a

  << { \voiceOne
    <f a>1
    <g a>1
    <f a>2 ~ <f c'>2
    c'2 \tuplet 3/2 { bf4 c bf }
    <f a>1
    <g a>1
    <f bf>1
    <g bf>2 <a cs>
  } \new Voice { \voiceTwo
    d,,1_\markup{ \italic { Ped. } }
    e1
    f1
    g1
    d1
    e1
    f1
    g2 a
  } \new Voice { \voiceFour
    s1*3
    g'1
    s1*4
  } >>

  << { \voiceOne
    <f a>1
    <g a>1
    <f a>2 ~ <f c'>2
    c'2 \tuplet 3/2 { bf4 c bf }
    <f a>1
    <g a>1
    <f bf>1
    <g c>2 <a cs>
  } \new Voice { \voiceTwo
    d,,1
    e1
    f1
    g1
    d1
    e1
    f1
    g2 a
  } \new Voice { \voiceFour
    s1*3
    g'1
    s1*4
  } >>

  << { \voiceOne
    <f a>1
    <g a>1
    <f a>2 ~ <f c'>2
    c'2 \tuplet 3/2 { bf4 c bf }
    <a e>2 ~ <a f>2
    <g a>1
    <f bf>1
    <g c>2 <a cs>
  } \new Voice { \voiceTwo
    d,,1
    e1
    f1
    g1
    d1
    e1
    f1
    g2 a
  } \new Voice { \voiceFour
    s1*3
    g'1
    s1*4
  } >>

  << { \voiceOne
    <f a>1
    <g a>1
    <f a>1
    c'2 \tuplet 3/2 { bf4 c bf }
    <a e>2 ~ <a f>2
    <g a>1
    <f bf>2 <a d> ~
    <g d'>2 <a cs>
  } \new Voice { \voiceTwo
    d,,1
    e1
    f1
    g1
    d1
    e1
    f1
    g2 a
  } \new Voice { \voiceFour
    s1*3
    g'1
    s1*4
  } >>

  << { \voiceOne
    <f a>2 <g a>
    <f a>2 <g c>
    <f a>2 <g a>
    <f a>2 <g c>
    <e a>2 <g bf>
    <e a>2 <f a>
    <f bf>2 <f d'>
    <bf d f> <a e'>
  } \new Voice { \voiceTwo
    d,,2 e
    f2 g
    d2 e
    f2 e
    d2 g
    c a
    bf c
    d e
  } >>

  << { \voiceOne
    d'2 r2
    s1*3
  } \new Voice { \voiceTwo
    <d, d,>1 ~
    <d d,>1 ~
    <d d,>1 ~
    <d d,>1
  } >> \oneVoice
  c2_\markup{ \italic { Man. } } bf
  a1
  <d d,>1 ~
  <d d,>1
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
