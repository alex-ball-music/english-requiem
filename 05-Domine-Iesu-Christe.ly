\version "2.20.0"
\language "english"
\include "05-Domine-Iesu-Christe_music.ly"

\header {
  title = "5. Domine, Iesu Christe"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

\score {
  <<
    \choirPart
    \pianoPart
    \context Staff {
      \set Score.instrumentEqualizer = #choral-instrument-equalizer
    }
  >>
  \layout { }
}

\score {
  <<
    <<\tempotrack>>
    \choirPart
    \pianoPart
    \context Staff {
      \set Score.instrumentEqualizer = #choral-instrument-equalizer
    }
  >>
  \midi { }
}

% Rehearsal MIDI files:
\book {
  \bookOutputSuffix "soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorVerse
    \midi { }
  }
}

\book {
  \bookOutputSuffix "bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassVerse
    \midi { }
  }
}
