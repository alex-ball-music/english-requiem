\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key g \major
  \time 4/4
  \tempo "Tranquillo" 4=72
}

tmControl = {
  s1*3
  \repeat volta 2 {
    s1*11 \bar"||"\mark\default
    s1*8 \bar"||"\mark\default
    s1*9
    %\once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
    \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
    \once \revert Score.RehearsalMark.stencil
    \mark \markup{ \small \italic { (opt. repeat) } }
  }
  s1 \bar"||"\mark\default
  s1*7
  \override TextSpanner.bound-details.left.text = "rall."
  s2.\startTextSpan s4\stopTextSpan
  \override TextSpanner.bound-details.left.text = "a tempo, rall."
  s4\startTextSpan s2.
  s4 s4\stopTextSpan s2\bar"|."
}

tempotrack = {
  s1*3
  \repeat volta 2 {
    s1*28
  }
  s1*8
  \tempo 4=64
  s2
  \tempo 4=60
  s2
  \tempo 4=68
  s4
  \tempo 4=64
  s4
  \tempo 4=60
  s4
  \tempo 4=56
  s4
  \tempo 4=52
  s1
}

pnControl = {
  s1\mp
  s1*31
  s1\mf
  s1*2
  s2 s2\mp
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1*3
  \repeat volta 2 {
    \once\override MultiMeasureRestText.self-alignment-X = #RIGHT
    R1^\markup{ \smcp { soprano solo } or \smcp { group } }

    b4\mp( d) c b8 a
    b4 g2 g8 a
    b4 g2 g8 a
    b4 g2 a4
    b4 d c b8 a
    b4 g2 g8 a
    b4 g2 g8 a
    b4 g2.

    R1*5
    r2. r8

    d8
    a'8 g a b c4 b8 c
    d8 d4 b8 a4 a8 d,
    a'8 b c d e4 d8 b
    a4 a r4

    \set Staff.shortInstrumentName = #"S."
    a4
    b4( d c) b8 a
    b4 g2 g8 a
    b4 g2 g8 a
    b4 g2 a4
    b2 g2
    a2. b4
    b1

    R1*2
  }
  \set Staff.shortInstrumentName = #""
  r2.^\markup{ \smcp { tutti } }

  d,4\mf
  a'4 a8 b c4 b8( c)
  d4 a4 r b
  a8( b) c d e4 d8( b)
  a4 a4 r

  a4\mp
  b4( d c) b8 a
  g2. a4
  b2( g2)
  a2. b4
  b1 ~
  2 r
}

alto = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*3
  \repeat volta 2 {
    \once\override MultiMeasureRestText.self-alignment-X = #RIGHT
    R1^\markup{ \smcp { alto solo } or \smcp { group } }

    d4\mp( f) e d8 c
    d4 b( c) c8 c
    d4 b( c) c8 c
    d4 b( c) c4
    d4 f e d8 c
    d4 b( c) c8 c
    d4 b( c) c8 c
    d4 b( c2)

    R1
    r2. r8

    d8
    a'8 g a b c4 b8 g
    e8 e4 g8 d4 d8 d
    a'8 g a b c4 b8 g
    d4 d2 r4

    R1*3
    r2.

    \set Staff.shortInstrumentName = #"A."
    c4
    d4( f e) d8 c
    d4 b( c) c8 c
    d4 b( c) c8 c
    d4 b( c) c4
    d2 b
    c2( d4) d4
    d1

    R1*2
  }
  \set Staff.shortInstrumentName = #""
  r2.^\markup{ \smcp { tutti } }

  d4\mf
  a'4 a8 b c4 b8( g)
  d4 d r d4
  a'4 a8 b c4 b8( g)
  d4 d r

  c4\mp
  d4( f e) d8 c
  b2( c4) c4
  d2( b)
  c2. d4
  d1 ~
  2 r
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1*3
  \repeat volta 2 {
    R1*20
    \set Staff.shortInstrumentName = #"T."
    R1*8
  }
  \set Staff.shortInstrumentName = #""
  r2.

  b4\mf
  a4 8 8 4 4
  4 4 r a4
  4 8 8 4 4
  4 4 r
  
  a4\mp
  g2 4 4
  2. 4
  1
  2 fs2
  g1 ~
  2 r
}

bass = \relative c {
  \global\dynamicUp
  % Music follows here.
  R1*3
  \repeat volta 2 {
    R1*20
    \set Staff.shortInstrumentName = #"B."
    R1*8
  }
  \set Staff.shortInstrumentName = #""  
  r2.

  d4\mf
  d4 8 8 4 4
  4 4 r d4
  4 8 8 4 4
  4 4 r

  d\mp
  g,2 4 4
  2. 4
  1
  d'2 2
  g,1 ~
  2 r
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Light shines in the dark -- ness
  for the up -- right,
  for the up -- right,
  for they will ne -- ver be sha -- ken,
  ne -- ver sha -- ken,
  ne -- ver sha -- ken.

  The righ -- teous will be held in e -- ter -- nal re -- mem -- brance;
  their head will be e -- xal -- ted for e -- ver.

  The souls of the righ -- teous,
  of the righ -- teous,
  of the righ -- teous,
  are in the hand of God.

  No tor -- ment will e -- ver touch them.
  No tor -- ment will e -- ver touch them.

  They seem to have died
  but they are at peace.

}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Light shines in the dark -- ness
  for the up -- right,
  for the up -- right,
  for they will ne -- ver be sha -- ken,
  ne -- ver sha -- ken,
  ne -- ver sha -- ken.

  The righ -- teous will be held in e -- ter -- nal re -- mem -- brance;
  their head will be e -- xal -- ted for e -- ver.

  The souls of the righ -- teous,
  of the righ -- teous,
  of the righ -- teous,
  are in the hand of God.

  No tor -- ment will e -- ver touch them.
  No tor -- ment will e -- ver touch them.

  They seem to have died
  but they are at peace.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  No tor -- ment will e -- ver touch them.
  No tor -- ment will e -- ver touch them.

  They seem to have died
  but they are at peace.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  No tor -- ment will e -- ver touch them.
  No tor -- ment will e -- ver touch them.

  They seem to have died
  but they are at peace.
}

right = \relative c'' {
  \global
  <g b,>2 <a c,>
  <b d,> <a c,>
  <g b,> <a c,>
  \repeat volta 2 {
    << { \voiceOne b4 ~ <g b> a ~ <e a> } \new Voice { \voiceTwo d2 c } >> \oneVoice
    <b' g d>4 <d f,> <c e,> <b d,>8 <a c,>
    <g b,>2 <a c,>
    <b d,> <a c,>
    <g b,> <a c,>
    <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
    <g b,>2 <a c,>
    <b d,> <a c,>
    <g b,> <g c,>
    <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
    <g b,>2 <b d,>
    <a d,>1 ~
    q1 ~
    q1 ~
    q1 ~
    q1 ~
    q1 ~
    q1 ~
    q2 <a c,>2
    <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
    <g b,>2 <a c,>
    <b d,> <a c,>
    <g b,> <a c,>
    <b d,> <g b,>
    <a c,> <a d,>
    <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
    <g b,>2 <a c,>
    <b d,> <a c,>
  }
  <g b,>2 <b d,>
  <a d,>1 ~
  q ~
  q ~
  q2 <a d, c>2
  <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
  <g b,>2 << { \voiceOne g4 a } \new Voice { \voiceTwo c,2 } >> \oneVoice
  <b' d,> <g b,>
  <a c,> q4 <b d,>4
  <b d,>4 <d f,> <c e,> <b d,>8 <a c,>
  <g b,>2 r
}

left = \relative c {
  \global
  g1 ~
  1 ~
  1 ~
  \repeat volta 2 {
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1 ~
    1
    d'2 2
    2 4 a4
    d2 2
    2 4 a4
    d2 2
    2 4 a4
    d2 2
    2 4 4
    g,1 ~
    1 ~
    1 ~
    1 ~
    1
    d'1
    g,1 ~
    1 ~
    1 ~
  }
  1
  d'2 2
  2 4 a4
  d2 2
  2 4 d,
  g1 ~
  1 ~
  1
  <d d'>1
  g1 ~
  2 r
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
