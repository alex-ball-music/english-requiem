\version "2.20.0"
\include "common.ly"
\include "common-resp.ly"

\header {
  title = "6. Sursum Corda"
  composer = "Alex Ball"
  % Remove default LilyPond tagline
  tagline = ##f
}

% Rehearsal MIDI files:
\score {
  \include "06-Sursum-Corda-v1.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "0"
  \score {
    \include "06-Sursum-Corda-v1.ly"
    \midi { }
  }
}

\include "06-Sursum-Corda-r1.ly"

\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}

\book {
  \bookOutputSuffix "1"
  \score {
    <<
      \choirPart
      \pianoPart
      \context Staff {
        \set Score.instrumentEqualizer = #choral-instrument-equalizer
      }
    >>
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "1-soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "1-alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "1-tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "1-bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassVerse
    \midi { \tempo 4=140 }
  }
}

\score {
  \include "06-Sursum-Corda-v2.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "2"
  \score {
    \include "06-Sursum-Corda-v2.ly"
    \midi { }
  }
}

\include "06-Sursum-Corda-r2.ly"

\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}

\book {
  \bookOutputSuffix "3"
  \score {
    <<
      \choirPart
      \pianoPart
      \context Staff {
        \set Score.instrumentEqualizer = #choral-instrument-equalizer
      }
    >>
    \midi { \tempo 4=140 }
  }
}


\book {
  \bookOutputSuffix "3-soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "3-alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "3-tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "3-bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassVerse
    \midi { \tempo 4=140 }
  }
}

\score {
  \include "06-Sursum-Corda-v3.ly"
  \include "common-greg.ly"
}

\book {
  \bookOutputSuffix "4"
  \score {
    \include "06-Sursum-Corda-v3.ly"
    \midi { }
  }
}

\include "06-Sursum-Corda-r3.ly"

\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}

\book {
  \bookOutputSuffix "5"
  \score {
    <<
      \choirPart
      \pianoPart
      \context Staff {
        \set Score.instrumentEqualizer = #choral-instrument-equalizer
      }
    >>
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "5-soprano"
  \score {
    \rehearsalMidi "soprano" "soprano sax" \sopranoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "5-alto"
  \score {
    \rehearsalMidi "alto" "soprano sax" \altoVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "5-tenor"
  \score {
    \rehearsalMidi "tenor" "tenor sax" \tenorVerse
    \midi { \tempo 4=140 }
  }
}

\book {
  \bookOutputSuffix "5-bass"
  \score {
    \rehearsalMidi "bass" "tenor sax" \bassVerse
    \midi { \tempo 4=140 }
  }
}
