\version "2.20.0"
\include "01-Requiem-Aeternam_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
