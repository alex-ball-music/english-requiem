\version "2.20.0"
\include "10-Lux-Aeterna_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
