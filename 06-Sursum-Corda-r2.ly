global = {
  \key g \major
  \time 4/4
}

pnControl = {
  s1
}

soprano = \relative c'' {
  \global
  % Music follows here.
  r2 a
  b d ~
  2 g,4 a
  b1 \bar"|."
}

alto = \relative c' {
  \global
  % Music follows here.
  r2 fs
  g2 g ~
  2 g4 fs
  g1
}

tenor = \relative c' {
  \global
  % Music follows here.
  r2 d
  d1
  e2 b4 a
  d1
}

bass = \relative c {
  \global
  % Music follows here.
  r2 d2
  g2 f
  e d
  g1
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  We lift them to the Lord.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  We lift them to the Lord.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  We lift them to the Lord.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  We lift them to the Lord.
}

right = \relative c'' {
  \global
  << { \voiceOne
    r2 a
    b d ~
    2 g,4 a
    b1 \bar "||"
  } \new Voice { \voiceTwo
    r2 fs
    g2 g ~
    <g e>2 g4 fs
    g1
  } >>
}

left = \relative c' {
  \global
  << { \voiceOne
    r2 d
    d1
    r2 b4 a
    d1
  } \new Voice { \voiceTwo
    d,,2 d'2
    g2 f
    e d
    g1
  } >> \oneVoice
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>

