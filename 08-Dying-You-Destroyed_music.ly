\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key a \minor
  \time 2/2
  \tempo 2=72
}

tmControl = {
  s1*4 \bar"||"
  s1*14 \bar"|."
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  a2.\f g4
  a2 e
  g a ~
  1\fermata
  a4\p g f e
  a2 g
  a1 ~
  2. r4
  d,4\mp e f g
  a2 b4( g)
  e1 ~
  4 r a2\mf\cresc
  c a ~
  1
  r2 c2 ~
  2 b4( a)
  d1\f
  d1
}

alto = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  a2.\f g4
  a2 e
  g a ~
  1\fermata
  e4\p e d e
  f2 d
  c2( d
  e2.) r4
  d4\mp c d e
  f2 e
  d( c
  b4) r
  e2\mf\cresc
  a1
  g
  f
  e2( g)
  fs1\f
  fs
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  a2.\f g4
  a2 e
  g a ~
  1\fermata
  c4\p b a g
  f2 g
  a2( b
  c2.) r4
  a4\mp g a c
  b2 g2
  g1(
  gs4) r a2\mf\cresc
  a2 c2 ~
  1
  r2 a2 ~
  2 b4( c)
  a1\f a1
}

bass = \relative c' {
  \global\dynamicUp
  % Music follows here.
  a2.\f g4
  a2 e
  g a ~
  1\fermata
  a,4\p a b c
  d2 b
  a1( ~
  2 g4) r
  f4\mp f f c'
  d2 e
  c2( d
  e4) r c2\mf\cresc
  f1
  e1
  d1
  c2 e4( a,)
  d1\f d1
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Praise to you, Lord Je -- sus: __
  Dy -- ing you de -- stroyed our death, __
  ri -- sing you re -- stored our life: __
  Lord Je -- sus, __ come in glo -- ry.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Praise to you, Lord Je -- sus: __
  Dy -- ing you de -- stroyed our death, __
  ri -- sing you re -- stored our life: __
  Lord Je -- sus, come in __ glo -- ry.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Praise to you, Lord Je -- sus: __
  Dy -- ing you de -- stroyed our death, __
  ri -- sing you re -- stored our life: __
  Lord Je -- sus, __ come in glo -- ry.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  Praise to you, Lord Je -- sus: __
  Dy -- ing you de -- stroyed our death, __
  ri -- sing you re -- stored our life: __
  Lord Je -- sus, come, come in __ glo -- ry.
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

