\version "2.20.0"

\layout {
  indent = #0
  short-indent = #0
  \context { \Score
    \remove "Bar_number_engraver"
  }
  \context {
    \Staff
    \remove Time_signature_engraver
  }
}
