# Texts for a Requiem Mass

## Traditional Roman Catholic

1. Intriot: Requiem Aeternam / Te Decet
2. Kyrie Eleison
3. Gradual: Requiem Aeternam / In Memoria (Optional)
4. Tract (in place of Alleluia): Absolve, Domine (Optional)
5. Sequence: Dies Irae
6. Offertory: Domine Iesu Christe
7. Sanctus
8. Benedictus
9. Agnus Dei
10. Communion: Lux aeterna

Other texts

-   Pie Jesu (motet, sometimes sung at the elevation - extracts from Dies Irae and Agnus Dei)
-   In Paradisum (sung as body leaves church)
-   Libera Me (beside coffin, between Requiem and burial)

## All Souls Requiem Missal

1. Intriot: Requiem Aeternam / Te Decet
2. Kyrie Eleison
3. Gradual hymn
4. Pie Jesu (at end of commemoration of faithful departed)
5. Offertory: Domine Iesu Christe
6. Sanctus
7. Benedictus
8. Acclamation
9. Agnus Dei
10. Communion: Libera Me
11. Recession: In Paradisum

## Common Worship funeral service within Communion

1. Introit
2. Hymn
3. Kyrie Eleison
4. Canticle
5. Gospel Alleluia
6. Offertory
7. Sanctus
8. Benedictus
9. Acclamation
10. Agnus Dei

## St Peters All Souls' Eucharist

1. Introit (before Greeting)
2. Kyrie Eleison (near prayers of penitence)
3. Gradual (after readings, before Gospel)
4. Gospel Alleluia
5. Commemoration of the faithful departed
6. Offertory (after Peace, before Eucharistic Prayer)
7. Sanctus
8. Benedictus
9. Acclamation
10. Agnus Dei (before Communion)
11. During communion

## Introits

### Requiem Aeternam

From 4 Esdras 2:34–35; Psalm 65:1-2

Requiem æternam dona eis, Domine:
et lux perpetua luceat eis.
Te decet hymnus, Deus, in Sion,
et tibi reddetur votum in Ierusalem:
exaudi orationem meam,
ad te omnis caro veniet.
Requiem æternam dona eis, Domine:
et lux perpetua luceat eis.

Eternal rest give unto them, O Lord,
and let perpetual light shine upon them.
A hymn, O God, becometh Thee in Zion;
and a vow shall be paid to Thee in Jerusalem:
hear my prayer;
all flesh shall come to Thee.
Eternal rest give unto them, O Lord,
and let perpetual light shine upon them.

Rest eternal grant unto them, O Lord: and let light perpetual shine upon them.
Thou, O God, art praised in Zion,
and unto thee shall the vow be performed in Jerusalem:
thou that hearest the prayer, unto thee shall all flesh come.
Rest eternal grant unto them, O Lord: and let light perpetual shine upon them.

## Kyrie

Kyrie, eleison.
Christe, eleison.
Kyrie, eleison.

Lord, have mercy.
Christ, have mercy.
Lord, have mercy.

You raise the dead to life in the Spirit:
Lord, have mercy.
Lord, have mercy.

You bring pardon and peace to the broken in heart:
Christ, have mercy.
Christ, have mercy.

You make one by your Spirit the torn and divided:
Lord, have mercy.
Lord, have mercy.

## Graduals

### Requiem Aeternam

From 4 Esdras 2:34–35; Psalm 112:7

Requiem æternam dona eis, Domine:
et lux perpetua luceat eis.
In memoria æterna erit iustus:
ab auditione mala non timebit.

Eternal rest give unto them, O Lord;
and let perpetual light shine upon them.
The just shall be in everlasting remembrance;
he shall not fear the evil hearing.

Rest eternal grant unto them, O Lord:
and let light perpetual shine upon them.
The righteous shall be had in everlasting remembrance:
he will not be afraid of any evil tidings.

The righteous will never be shaken:
they will be kept in remembrance forever.
They will not live in fear of bad tidings:
because they trust with steadfast heart in the Lord.

### Anglican

Psalms 6, 23, 25, 27, 32, 38.9-end, 42, 90, 116, 118.4-end, 121, 139.

A Song of the Redeemer (Isaiah 61.1-3,11b,4; 62.12)

-   Proclaim the time of the Lord’s favour,
    and comfort all who grieve.

The Song of Manasseh (Manasseh 1a,2,4,6,7,9a,9c,11,12,14b,15b)

-   Full of compassion and mercy and love
    is God, the Most High, the Almighty.

A Song of the Righteous (Wisdom 3.1,2a,3b-8)

-   God has found the righteous worthy
    and their hope is of immortality.

A Song of the Justified (Romans 4.24,25; 5.1-5,8,9,11)

-   We are justified by faith,
    we have peace with God through our Lord Jesus Christ.

A Song of God’s Children (Romans 8.2,14,15b-19)

-   The Spirit of the Father,
    who raises Christ Jesus from the dead,
    gives life to the people of God.

A Song of Faith (1 Peter 1.3-5,18,19,21)

-   God raised Christ from the dead,
    the Lamb without spot or stain.

A Song of the Redeemed (Revelation 7.9,10,14b-17)

-   Salvation belongs to our God,
    who will guide us to springs of living water.

A Song of the Lamb (Revelation 19.1b,2b,5b,6b,7,9b)

-   Let us rejoice and exult
    and give glory and homage to our God.

A Song of St Anselm

-   Gather your little ones to you, O God,
    as a hen gathers her brood to protect them.

## Tracts

### Absolve, Domine

Absolve, Domine,
animas omnium fidelium defunctorum
ab omni vinculo delictorum.
Et gratia tua illis succurrente,
mereantur evadere iudicium ultionis.
Et lucis æternae beatitudine perfrui.

Absolve, O Lord,
the souls of all the faithful departed
from every bond of sin.
And by the help of Thy grace
may they be enabled to escape the avenging judgment.
And enjoy the bliss of everlasting light.

Absolve, O Lord, the souls of all the faithful departed:
from every chain of sin.
And by the help of thy grace,
may they be worthy to escape the judgment of condemnation:
and attain the fruition of everlasting light.

### Sicut cervus

Psalm 42

1.  Like as the hart desireth the water-brooks: so longeth my soul after thee, O God.
2.  My soul is athirst for God, yea, even for the living God: when shall I come to appear before the presence of God?
3.  My tears have been my meat day and night: while they daily say unto me, Where is now thy God?
4.  Now when I think thereupon, I pour out my heart by myself: for I went with the multitude, and brought them forth into the house of God;
5.  In the voice of praise and thanksgiving: among such as keep holy-day.
6.  Why art thou so full of heaviness, O my soul: and why art thou so disquieted within me?
7.  Put thy trust in God: for I will yet give him thanks for the help of his countenance.
8.  My God, my soul is vexed within me: therefore will I remember thee concerning the land of Jordan, and the little hill of Hermon.
9.  One deep calleth another, because of the noise of the water-pipes: all thy waves and storms are gone over me.
10.  The Lord hath granted his loving-kindness in the day-time: and in the night-season did I sing of him, and made my prayer unto the God of my life.
11.  I will say unto the God of my strength, Why hast thou forgotten me: why go I thus heavily, while the enemy oppresseth me?
12.  My bones are smitten asunder as with a sword: while mine enemies that trouble me cast me in the teeth;
13.  Namely, while they say daily unto me: Where is now thy God?
14.  Why art thou so vexed, O my soul: and why art thou so disquieted within me?
15. O put thy trust in God: for I will yet thank him, which is the help of my countenance, and my God.

### Anglican 1

Alleluia, alleluia.
God so loved the world
that he gave his only Son.
Alleluia.

### Anglican 2

Alleluia, alleluia.
Blessed are those who die in the Lord
for they rest from their labour.
Alleluia.

### Anglican 3

Alleluia, alleluia.
‘It is the will of him who sent me,’ says the Lord,
‘that I should lose none of all that he has given me,
but raise them up on the last day.’

## Dies Irae

Dies irae, dies illa
Solvet saeclum in favilla,
Teste David cum Sibylla.

Quantus tremor est futurus,
Quando judex est venturus,
Cuncta stricte discussurus!

Tuba mirum spargens sonum
Per sepulcra regionum,
Coget omnes ante thronum.

Mors stupebit et natura,
Cum resurget creatura,
Judicanti responsura.

Liber scriptus proferetur,
In quo totum continetur,
Unde mundus judicetur.

Judex ergo cum sedebit,
Quidquid latet apparebit.
Nil inultum remanebit.

Quid sum miser tunc dicturus?
Quem patronum rogaturus,
Cum vix justus sit securus?


Rex tremendae majestatus
qui salvandos salvas gratis
sale me, fons pietatis

Recordare, Jesu pie,
Quod sum causa tuae viae:
Ne me perdas illa die.

Quaerens me, sedisti, lassus;
Redemisti crucem passus;
Tantus labor non sit cassus.

Juste Judex ultionis,
Donum fac remissionis
Ante diem rationis.

Ingemisco tanquam reus,
Culpa rubet vultus meus;
Supplicanti parce, Deus.

Qui Mariam absolvisti,
Et latronem exaudisti,
Mihi quoque spem dedisti.

Preces meae non sunt dignae,
Sed tu, bonus, fac benigne,
Ne perenni cremer igne.

Inter oves locum praesta,
Et ab hoedis me sequestra,
Statuens in parte dextra.

Confutatis maledictis
Flammis acribus addictis,
Voca me cum benedictus.

Oro supplex et acclinis,
Cor contritum quasi cinis,
Gere curam mei finis.


Lacrimosa dies illa,
Qua resurget ex favilla

Judicandus homo reus.
Huic ergo parce, Deus:

Pie Jesu Domine:
Dona eis requiem. Amen.


This day, this day of wrath
shall consume the world in ashes,
as foretold by David and the Sibyl.

What trembling there will be
When the judge shall come
to weigh everything strictly!

The trumpet, scattering its awful sound
Across the graves of all lands
Summons all before the throne.

Death and nature shall be stunned
When mankind arises
To render account before the judge.

The written book shall be brought
In which all is contained
Whereby the world shall be judged

When the judge takes his seat
all that is hidden shall appear
Nothing will remain unavenged.

What shall I, a wretch, say then?
To which protector shall I appeal
When even the just man is barely safe?


King of awful majesty
You freely save those worthy of salvation
Save me, found of pity.

Remember, gentle Jesus
that I am the reason for your time on earth,
do not cast me out on that day

Seeking me, you sank down wearily,
you saved me by enduring the cross,
such travail must not be in vain.

Righteous judge of vengeance,
award the gift of forgiveness
before the day of reckoning.

I groan as one guilty,
my face blushes with guilt;
spare the suppliant, O God.

Thou who didst absolve Mary (Magdalene)
and hear the prayer of the thief
hast given me hope, too.

My prayers are not worthy,
but Thou, O good one, show mercy,
lest I burn in everlasting fire,

Give me a place among the sheep,
and separate me from the goats,
placing me on Thy right hand.

When the damned are confounded
and consigned to keen flames,
call me with the blessed.

I pray, suppliant and kneeling,
a heart as contrite as ashes;
take Thou my ending into Thy care.


That day is one of weeping,
on which shall rise again from the ashes
the guilty man, to be judged.

Therefore spare this one, O God,
merciful Lord Jesus:
Give them rest. Amen.

## Offertories

### Domine Iesu Christe

Domine Iesu Christe, Rex gloriæ,
libera animas omnium fidelium defunctorum
de pœnis inferni et de profundo lacu:
libera eas de ore leonis,
ne absorbeat eas tartarus,
ne cadant in obscurum:

Lord Jesus Christ, King of glory,
deliver the souls of all the faithful departed
from the pains of hell and from the bottomless pit:
deliver them from the lion's mouth,
that hell swallow them not up,
that they fall not into darkness:

O Lord Jesus Christ, King of Majesty,
deliver the souls of all the faithful departed from the hand of hell,
and from the pit of destruction:
deliver them from the lion's mouth,
that the grave devour them not;
that they go not down to the realms of darkness:

*Ending 1:*

sed signifer sanctus Michael
repræsentet eas in lucem sanctam:
Quam olim Abrahæ promisisti, et semini eius.

but let the standard-bearer holy Michael
lead them into that holy light:
Which Thou didst promise of old to Abraham and to his seed.

but let Michael, the holy standard-bearer,
make speed to restore them to the brightness of glory:
which thou hast promised in ages past to Abraham and his seed.

*Ending 2:*

Hostias et preces tibi, Domine,
laudis offerimus:
tu suscipe pro animabus illis,
quarum hodie memoriam facimus:
fac eas, Domine, de morte transire ad vitam.
Quam olim Abrahæ promisisti, et semini eius.

We offer to Thee, O Lord,
sacrifices and prayers:
do Thou receive them in behalf of those souls
of whom we make memorial this day.
Grant them, O Lord, to pass from death to that life,
Which Thou didst promise of old to Abraham and to his seed.

Sacrifice and prayer do we offer to thee, O Lord:
do thou accept them for the souls departed,
in whose memory we make this oblation:
and grant them, Lord, to pass from death unto life:
which thou hast promised in ages past to Abraham and his seed.

### Anglican

May all who are called to a place at your table
follow in the way that leads to the unending feast of life.
Amen.

## Sanctus

Sanctus, Sanctus, Sanctus
Dominus Deus Sabaoth.
Pleni sunt cæli et terra gloria tua.
Hosanna in excelsis.

Holy, holy, holy Lord,
God of power and might,
heaven and earth are full of your glory.
Hosanna in the highest.

## Benedictus

Benedictus qui venit in nomine Domini.
Hosanna in excelsis.

Blessed is he who comes in the name of the Lord.
Hosanna in the highest.

## Acclamation

Praise to you, Lord Jesus:
dying you destroyed our death,
rising you restored our life:
Lord Jesus, come in glory.

## Agnus Dei

Agnus Dei, qui tollis peccata mundi: dona eis requiem.
Agnus Dei, qui tollis peccata mundi: dona eis requiem.
Agnus Dei, qui tollis peccata mundi: dona eis requiem sempiternam.

Lamb of God, Who takest away the sins of the world, grant them rest.
Lamb of God, Who takest away the sins of the world, grant them rest.
Lamb of God, Who takest away the sins of the world, grant them eternal rest.

Lamb of God,
you take away the sin of the world,
have mercy on us.

Lamb of God,
you take away the sin of the world,
have mercy on us.

Lamb of God,
you take away the sin of the world,
grant us peace.

O Lamb of God, that takest away the sins of the world,
    grant them rest.
O Lamb of God, that takest away the sins of the world,
    grant them rest.
O Lamb of God, that takest away the sins of the world,
    grant them rest eternal.

### Lux Aeterna

Lux æterna luceat eis, Domine:
Cum Sanctis tuis in æternum:
quia pius es.
Requiem æternam dona eis, Domine:
et lux perpetua luceat eis.
Cum Sanctis tuis in æternum:
quia pius es.

May light eternal shine upon them, O Lord,
with Thy Saints for evermore:
for Thou art gracious.
Eternal rest give to them, O Lord,
and let perpetual light shine upon them:
With Thy Saints for evermore,
for Thou art gracious.

### Pie Jesu

Pie Iesu Domine, dona eis requiem.
Dona eis requiem sempiternam.

Merciful Lord Jesus, grant them rest;
grant them eternal rest.

### Libera Me

Libera me, Domine, de morte æterna, in die illa tremenda:
Quando cæli movendi sunt et terra:
Dum veneris iudicare sæculum per ignem.

Tremens factus sum ego, et timeo,
dum discussio venerit, at que ventura ira.
Quando cæli movendi sunt et terra.

Dies illa, dies iræ, calamitatis et miseriæ,
dies magna et amara valde.
Dum veneris iudicare sæculum per ignem.

Requiem æternam dona eis, Domine: et lux perpetua luceat eis.

---

Deliver me, O Lord, from death eternal in that awful day.
When the heavens and the earth shall be moved:
When Thou shalt come to judge the world by fire.

Dread and trembling have laid hold on me, and I fear exceedingly
because of the judgment and of the wrath to come.
When the heavens and the earth shall be moved.

O that day, that day of wrath, of sore distress and of all wretchedness,
that great day and exceeding bitter.
When Thou shalt come to judge the world by fire.

Eternal rest grant unto them, O Lord, and let perpetual light shine upon them.

---

Deliver me, O Lord, from death eternal in that day of trembling:
when heaven and earth shall be shaken:
when thou shalt come to judge the world by fire.

V. Trembling taketh hold upon me, and fearfulness,
as the sifting draweth on and the wrath to come:
when heaven and earth shall be shaken.

R. Ah, that day, that day of anger, of calamity and misery;
ah that great day, and exceeding bitter!
When thou shalt come to judge the world by fire.

V. Rest eternal grant unto them, O Lord: and let light perpetual shine upon them.

Deliver me, O Lord, from death eternal in that day of trembling:
when heaven and earth shall be shaken:
when thou shalt come to judge the world by fire.

### Russian Kontakion

Give rest, O Christ, to your servant with the saints:
where sorrow and pain are no more,
neither sighing, but life everlasting.

You only are immortal, the creator and maker of all:
and we are mortal, formed from the dust of the earth,
and unto earth shall we return.
For so you ordained when you created me, saying:
‘Dust you are and to dust you shall return.’
All of us go down to the dust,
yet weeping at the grave, we make our song:
Alleluia, alleluia, alleluia.

Give rest, O Christ, to your servant with the saints:
where sorrow and pain are no more,
neither sighing, but life everlasting.

### In Paradisum

Christ is risen from the dead, trampling down death by death, and giving life to those in the tomb.

The Sun of Righteousness is gloriously risen, giving light to those who sat in darkness and in the shadow of death.

The Lord will guide our feet into the way of peace, having taken away the sin of the world.

Christ will open the kingdom of heaven to all who believe in his Name, saying, Come, O blessed of my Father; inherit the kingdom prepared for you.


In paradisum deducant angeli;
in tuo adventu suscipiant te martyrus
et perducant te in civitatem sanctam Jerusalem.

Chorus angelorum te suscipat
et cum Lazaro, quondam paupere,
aeternam habeas requiem.

Into paradise may the angels lead thee;
and at thy coming may the martyrs receive thee,
and bring thee into the holy city Jerusalem.

May the choirs of angels receive thee,
and mayest thou, with Lazarus once poor,
have everlasting rest.
