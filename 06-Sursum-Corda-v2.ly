\relative <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c''
      {
        \set Score.tempoHideNote = ##t
        \tempo 4=180
        a2(
        \tempo 4=140
        b4 c)
        \tempo 4=90
        b
        \tempo 4=140
        a( b)
        \tempo 4=180
        a2( g)
        \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { Lift up your hearts. }
  >>
>>
