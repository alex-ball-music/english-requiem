\version "2.20.0"
\include "07-Sanctus-Benedictus_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
