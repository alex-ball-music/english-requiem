global = {
  \key g \major
  \time 4/4
}

pnControl = {
  s1
}

soprano = \relative c'' {
  \global
  % Music follows here.
  r2 a4 b
  c1 ~
  2 a4 b
  c1(
  b2) a
  d1 \bar"|."
}

alto = \relative c' {
  \global
  % Music follows here.
  r2 fs4 g
  a2 e4 fs
  g1 ~
  g2 g4 fs
  e2 fs
  g1
}

tenor = \relative c' {
  \global
  % Music follows here.
  r2 d4 d
  e2 c4 d
  e1 ~
  e2 e4 d
  c2 c
  b1
}

bass = \relative c {
  \global
  % Music follows here.
  r2 d4 b
  a1 ~
  a2 e'4 d
  c1(
  d2) d
  g1
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  It is right __ to give thanks __ and praise.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  It is right, it is right __ to give thanks and praise.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  It is right, it is right __ to give thanks and praise.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  It is right __ to give thanks __ and praise.
}

right = \relative c'' {
  \global
  << { \voiceOne
    r2 a4 b
    c1 ~
    2 a4 b
    c1(
    b2) a
    d1 \bar"||"
  } \new Voice { \voiceTwo
    r2 <fs, d>4 <g d>
    <a e>2 <e c>4 <fs d>
    <g e>1 ~
    q2 <g e>4 <fs d>
    <e c>2 <fs c>
    g1
  } >>
}

left = \relative c, {
  d2 d'4 b
  a1 ~
  a2 e'4 d
  c1(
  d2) d
  <g b g,>1
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>

