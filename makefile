SHELL=bash
PWD   = $(shell pwd)
TEMP := $(shell mktemp -d -t tmp.XXXXXXXXXX)
OUTDIR=releases/$(shell date +%F)
MOVEMENTS1=01-Requiem-Aeternam 02-Kyrie-Eleison 03-Absolve-Domine 04-In-Memoria-Aeterna 05-Domine-Iesu-Christe
MOVEMENTS2=07-Sanctus-Benedictus 08-Dying-You-Destroyed 09-Agnus-Dei 10-Lux-Aeterna
MOVEMENTS=$(MOVEMENTS1) $(MOVEMENTS2)
SURSUM=06-Sursum-Corda
SURSA=$(SURSUM)-r1 $(SURSUM)-v1 $(SURSUM)-r2 $(SURSUM)-v2 $(SURSUM)-r3 $(SURSUM)-v3
SURSAMID=$(SURSUM)-0 $(SURSUM)-1 $(SURSUM)-2 $(SURSUM)-3 $(SURSUM)-4 $(SURSUM)-5

SURSAMIDI=$(SURSAMID:%=%.midi)
FULLMP3 := $(MOVEMENTS:%=%.mp3) $(SURSUM:%=%.mp3)
REHEARSEMP3 := $(MOVEMENTS:%=%-soprano.mp3) $(MOVEMENTS:%=%-alto.mp3) $(MOVEMENTS:%=%-tenor.mp3) $(MOVEMENTS:%=%-bass.mp3) $(SURSUM:%=%-soprano.mp3) $(SURSUM:%=%-alto.mp3) $(SURSUM:%=%-tenor.mp3) $(SURSUM:%=%-bass.mp3)

.PHONY: release tidy

all: an-english-requiem.pdf

release: $(OUTDIR)/an-english-requiem.pdf $(OUTDIR)/an-english-requiem.mp3 $(OUTDIR)/an-english-requiem_rehearsal-soprano.zip $(OUTDIR)/an-english-requiem_rehearsal-alto.zip $(OUTDIR)/an-english-requiem_rehearsal-tenor.zip $(OUTDIR)/an-english-requiem_rehearsal-bass.zip $(OUTDIR)/an-english-requiem_midi.zip $(OUTDIR)/an-english-requiem_mp3.zip

$(OUTDIR):
	mkdir -p $(OUTDIR)

$(OUTDIR)/an-english-requiem.pdf: $(OUTDIR) an-english-requiem.pdf
	cp an-english-requiem.pdf $(OUTDIR)

an-english-requiem.pdf: an-english-requiem.tex common.ly common-greg.ly common-resp.ly $(MOVEMENTS:%=%_book.ly) $(MOVEMENTS:%=%_music.ly) $(SURSA:%=%.ly)
	latexmk -pdf -lualatex -quiet --shell-escape --interaction=nonstopmode $<

$(OUTDIR)/an-english-requiem.mp3: $(OUTDIR) $(MOVEMENTS:%=%.midi) $(SURSUM).midi
	./midi2mp3.py -o $(OUTDIR)/an-english-requiem.mp3 -g 4 $(MOVEMENTS1:%=%.midi) $(SURSUM).midi $(MOVEMENTS2:%=%.midi)

$(OUTDIR)/an-english-requiem_mp3.zip: $(addsuffix .mp3, $(MOVEMENTS) $(SURSUM))
	mkdir $(TEMP)/an-english-requiem_mp3
	cp $^ $(TEMP)/an-english-requiem_mp3
	cd $(TEMP); zip -Drq $(PWD)/$@ an-english-requiem_mp3

$(OUTDIR)/an-english-requiem_rehearsal-%.zip: $(addsuffix -%.mp3, $(MOVEMENTS) $(SURSUM))
	mkdir $(TEMP)/an-english-requiem_rehearsal-$*
	cp $^ $(TEMP)/an-english-requiem_rehearsal-$*
	cd $(TEMP); zip -Drq $(PWD)/$@ an-english-requiem_rehearsal-$*

$(FULLMP3): %.mp3: %.midi
	./midi2mp3.py -o $@ $<

$(REHEARSEMP3): %.mp3: %.midi
	./midi2mp3.py -o $@ $<

$(OUTDIR)/an-english-requiem_midi.zip:
	mkdir $(TEMP)/midi
	cp $(MOVEMENTS:%=%.midi) $(SURSAMID:%=%.midi) $(TEMP)/midi
	cd $(TEMP); zip -Drq $(PWD)/$@ midi

$(SURSAMIDI) &: $(SURSUM).ly $(SURSA:%=%.ly)
	lilypond $<

$(SURSUM).midi: $(SURSAMID:%=%.midi)
	./midi2mp3.py -m $(SURSUM).midi -g 1 -t 0 $(SURSAMID:%=%.midi)

$(SURSUM)-soprano.midi: $(SURSUM)-0.midi
	./midi2mp3.py -m $@ -g 1 -t 0 $(SURSUM)-0.midi $(SURSUM)-1-soprano.midi $(SURSUM)-2.midi $(SURSUM)-3-soprano.midi $(SURSUM)-4.midi $(SURSUM)-5-soprano.midi

$(SURSUM)-alto.midi: $(SURSUM)-0.midi
	./midi2mp3.py -m $@ -g 1 -t 0 $(SURSUM)-0.midi $(SURSUM)-1-alto.midi $(SURSUM)-2.midi $(SURSUM)-3-alto.midi $(SURSUM)-4.midi $(SURSUM)-5-alto.midi

$(SURSUM)-tenor.midi: $(SURSUM)-0.midi
	./midi2mp3.py -m $@ -g 1 -t 0 $(SURSUM)-0.midi $(SURSUM)-1-tenor.midi $(SURSUM)-2.midi $(SURSUM)-3-tenor.midi $(SURSUM)-4.midi $(SURSUM)-5-tenor.midi

$(SURSUM)-bass.midi: $(SURSUM)-0.midi
	./midi2mp3.py -m $@ -g 1 -t 0 $(SURSUM)-0.midi $(SURSUM)-1-bass.midi $(SURSUM)-2.midi $(SURSUM)-3-bass.midi $(SURSUM)-4.midi $(SURSUM)-5-bass.midi

%-soprano.midi %-alto.midi %-tenor.midi %-bass.midi %.midi: %.ly %_music.ly
	lilypond $<

tidy:
	rm -rf tmp-ly/

clean: tidy
	rm -f an-english-requiem.pdf
	rm -f *.aux
	rm -f $(MOVEMENTS:%=%.pdf) $(SURSUM:%=%.pdf)
	rm -f $(MOVEMENTS:%=%.midi) $(MOVEMENTS:%=%-soprano.midi) $(MOVEMENTS:%=%-alto.midi) $(MOVEMENTS:%=%-tenor.midi) $(MOVEMENTS:%=%-bass.midi) $(SURSUM:%=%.midi) $(SURSAMID:%=%.midi)
	rm -f $(MOVEMENTS:%=%-soprano.mp3) $(MOVEMENTS:%=%-alto.mp3) $(MOVEMENTS:%=%-tenor.mp3) $(MOVEMENTS:%=%-bass.mp3) $(SURSUM:%=%-soprano.mp3) $(SURSUM:%=%-alto.mp3) $(SURSUM:%=%-tenor.mp3) $(SURSUM:%=%-bass.mp3)
