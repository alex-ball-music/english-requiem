\version "2.20.0"
\include "09-Agnus-Dei_music.ly"

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
    \pianoPart
  >>
  \layout { }
}
