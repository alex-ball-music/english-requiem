\relative <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c''
      {
        \set Score.tempoHideNote = ##t
        \tempo 4=135
        b4
        \tempo 4=180
        a( g)
        \tempo 4=225
        b( a b)
        \tempo 4=180
        c( b)
        \tempo 4=135
        a g
        \tempo 4=180
        g( a)
        \tempo 4=135
        b
        \tempo 4=90
        a
        \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { Let us give thanks to the Lord our God. }
  >>
>>
