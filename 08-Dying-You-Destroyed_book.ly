\version "2.20.0"
\include "08-Dying-You-Destroyed_music.ly"

\paper {
  system-system-spacing.padding = #8
}

\markup{ \vspace #1 }
\score {
  <<
    \choirPart
  >>
  \layout { }
}
