\version "2.20.0"
\language "english"
\include "common.ly"

global = {
  \key e \minor
  \time 6/4
  \tempo "Lamentoso" 4=72
}

tmControl = {
  s1.*13 \bar"||"\mark\default
  s1.*8 \bar"||"\mark\default
  s1.*10 \bar"|."
}

pnControl = {
  s1.\mf
  s1.*7
  s1.\>
  s1.\p
  s2. s2 s4\mp
  s1.
  s2. s2.\<

  s1.\mf
  s1.\<
  s1.\f
  s2. s4 s2\>
  s1.\mp
  s2. s2 s4\mf
  s1.*8
  s1.\>
  s1.\p\<
  s1.
  s1.\f
}

soprano = \relative c'' {
  \global\dynamicUp
  % Music follows here.
  R1.*2

  r4 b\mf g a( fs) r4
  r4 b g a( fs) r4
  e4 fs g fs2 d4
  g4 a b a2 r4
  r4 b g a( fs) r4
  r4 b g a( fs) r4
  R1.

  g2.\p a2.
  e2. ~ 4 r2
  R1.*2

  r4 b'4\mf c a2 r4
  b4\< c d d\f d d
  e4( d) c d c b
  b2( c4 b2) r4

  g2.\mp a2.
  e2. ~ 4 r2
  R1.*2

  r4 b'\mf g a( fs) r4
  r4 b g a( fs) r4
  e4 fs g fs2 d4
  g4 a b a2 r4
  r4 b g a( fs) r4
  r4 b g a( fs) r4
  R1.

  fs2.\p\< g2.
  g4( a) b a2 fs4
  e1.\f
}

alto = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1.*2

  e2\mf e4 fs( ds) r4
  e2 e4 fs( ds) r4
  r2. d4 e fs
  g2 g4 fs e ds
  e4 e e fs( ds) r4
  e2 e4 fs( ds) r4
  R1.

  d2.\p fs2.
  e2. ~ 4 r2
  R1.*2

  r4 g4\mf a fs2 r4
  g4\< a b a\f a b
  c4( b) a b a g
  fs2. ~ 2 r4

  d2.\mp fs2.
  e2. ~ 4 r2
  R1.*2

  e2\mf e4 fs( ds) r4
  e2 e4 fs( ds) r4
  r2. d4 e fs
  g2 g4 fs e ds
  e4 e e fs( ds) r4
  e2 e4 fs( ds) r4
  R1.

  ds2.\p\< e2.
  e2 g4 fs2 d4
  b1.\f
}

tenor = \relative c' {
  \global\dynamicUp
  % Music follows here.
  R1.*2

  r4 b\mf d c( b) r
  r4 b d c( b) r
  b b c a2 d4
  d4 c b ds( cs b)
  r4 b d c( b) r
  r4 b d c( b) r
  R1.

  b2.\p d2.
  g,2. ~ 4 r2
  R1.*2

  r4 b4\mf e4 d2 r4
  d4\< d e fs2\f d4
  c2. b2 cs4
  ds2. ~ 2 r4

  b2.\mp d2.
  g,2. ~ 4 r2
  R1.*2

  r4 b\mf d c( b) r
  r4 b d c( b) r
  b b c a2 d4
  d4 c b ds( cs b)
  r4 b d c( b) r
  r4 b d c( b) r
  R1.

  b2.\p\< b2.
  c2 b4 a2 a4
  gs1.\f
}

bass = \relative c {
  \global\dynamicUp
  % Music follows here.
  R1.
  r2. r2

  b4\mf
  e2 e4 a,4( b) b
  e2 e4 a,4( b4) r
  g' fs e d2 d4
  b4 a g b2

  b4
  e2 e4 a,4( b) b
  e2 e4 a,4( b4) r
  R1.

  g2.\p d'2.
  e2. ~ 4 r2
  R1.
  r2. r2

  d4\mf
  g2 g4 d2 r4
  b4\< a g d'2\f d4
  c2( e4) g2 g,8( a)
  b2. ~ 2 r4

  g2.\mp d'2.
  e2. ~ 4 r2
  R1.
  r2. r2

  b4\mf
  e2 e4 a,4( b) b
  e2 e4 a,4( b4) r
  g' fs e d2 d4
  b4 a g b2

  b4
  e2 e4 a,4( b) b
  e2 e4 a,4( b4) r
  R1.

  b2.\p\< e2.
  c2 c4 d2 fs4
  e1.\f
}

sopranoVerse = \lyricmode {
  % Lyrics follow here.
  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world.
  Lamb of God,
  Lamb of God,
  grant them rest. __

  Lamb of God,
  you take a -- way, take a -- way the sin of the world. __
  Grant them rest. __

  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world.
  Lamb of God,
  Lamb of God,
  grant them e -- ver -- last -- ing rest.
}

altoVerse = \lyricmode {
  % Lyrics follow here.
  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world.
  Lamb of God,
  Lamb of God,
  grant them rest. __

  Lamb of God,
  you take a -- way, take a -- way the sin of the world. __
  Grant them rest. __

  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world.
  Lamb of God,
  Lamb of God,
  grant them e -- ver -- last -- ing rest.
}

tenorVerse = \lyricmode {
  % Lyrics follow here.
  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world. __
  Lamb of God,
  Lamb of God,
  grant them rest. __

  Lamb of God,
  you take a -- way the sin of the world. __
  Grant them rest. __

  Lamb of God,
  Lamb of God,
  you take a -- way the sin of the world. __
  Lamb of God,
  Lamb of God,
  grant them e -- ver -- last -- ing rest.
}

bassVerse = \lyricmode {
  % Lyrics follow here.
  O Lamb of God,
  O Lamb of God,
  you take a -- way the sin of the world.
  O Lamb of God,
  O Lamb of God,
  grant them rest. __

  O Lamb of God,
  you take a -- way the sin __ of the world. __
  Grant them rest. __

  O Lamb of God,
  O Lamb of God,
  you take a -- way the sin of the world.
  O Lamb of God,
  O Lamb of God,
  grant them e -- ver -- last -- ing rest.
}

right = \relative c' {
  \global
  << { \voiceOne
    e8 g b g b g a4 g fs
    e8 g b g b g <a c>4 <a cs>4 <b ds>
    <e b g>8 g, b g <b d,> b a4 fs g
    e8 g b g b g a4 fs2
    e4 fs g a8 fs g a b c
    <b d,>4 <a c,> <b d,> a2.
    e8 g b g b g a4 fs g
    e8 g b g b g a4 b ds
    e4 b g a b a
  } \new Voice { \voiceTwo
    b,2 d4 c4 cs ds
    b2 d4 fs2.
    r2. c4 ds2
    b2 d4 c4 ds2
    b2 c4 d e fs
    g2. <fs ds>4 <e cs> <fs ds>
    b,2 d4 c4 ds2
    b2 d4 c4 fs2
  } >> \oneVoice
  <g d b>2. <a fs d>2.

  << { \voiceOne
    e8 g b g e g e2 r4
    e8 g b g b g a4 g fs
    e8 g b g b g a4 g fs

    g8 b d b d b fs4 g a
    b4 c d a8 d fs d fs d
    e4 d c d c b
    fs8 b ds b c ds fs,8 b ds4 a
  } \new Voice { \voiceTwo
    b,2. b2 r4
    b2 d4 c4 cs ds
    b2 d4 c4 d2

    d2 e4 d4 e fs
    g4 <a d,> <b e,> fs2 b4
    c4 b a b a <g cs,>
    ds1.
  } >> \oneVoice
  <g d b>2. <a fs d>2.

  << { \voiceOne
    e8 g b g e g e2.
  } \new Voice { \voiceTwo
    b1.
  } >> \oneVoice

  e'8 g b4 g a fs2
  e8 g b4 d c b2

  << { \voiceOne
    e,,8 g b g b g a4 fs g
    e8 g b g b g a4 fs2
    e4 fs g a8 fs g a b c
    <b d,>4 <a c,> <b d,> a2.
    e8 g b g b g a4 fs g
    e8 g b g b g a4 b ds
    e4 b g a b a8( g)
  } \new Voice { \voiceTwo
    r2 d4 c4 ds2
    b2 d4 c4 ds2
    b2 c4 d e fs
    g2. <fs ds>4 <e cs> <fs ds>
    b,2 d4 c4 ds2
    b2 d4 c4 fs2
  } >> \oneVoice
  <fs ds b>2. <g e b>
  << { \voiceOne
    g4 a b a c d
    e1.
  } \new Voice { \voiceTwo
    e,2. <a fs>2.
    <b gs>1.
  } >> \oneVoice
}

left = \relative c {
  \global
  e2. a4 b b,
  e2. a4 b b,

  <e b'>2. a4 b b,
  e2. a4 b b,
  <e g>4 fs e d c a
  g a g b b' b,
  e2. a4 b b,
  e2. a4 b b,
  <e b'>2. <d a'>2.

  <g, g'>2. <a a'>4 <d a'>2
  e2. e,2 b'4

  e2. a4 b b,
  e2. a,4 b a4

  g2 g4 d2.
  b'4 a g d'2 d,4
  c2( e4) g2 g8( a)
  b1.

  <g g'>2. <a a'>4 <d a'>2
  e2. ~ 2 b4

  << { \voiceOne
    b'4 d8 b d b <c e>4 <cs fs> <ds fs>
    b4 d8 b d b <c e>4 <cs fs> <ds fs>
  } \new Voice { \voiceTwo
    e,2._\markup{ \italic { Ped. } } a,4 b b
    e2. a,4 b b
  } >> \oneVoice

  <e b'>2._\markup{ \italic { Man. } } a4 b b,
  e2. a4 b b,
  <e g>4 fs e d c a
  g a g b b' b,
  e2. a4 b b,
  e2. a4 b b,
  <e b'>2. <d a'>2.
  <b b'> <e b'>
  << { \voiceOne
    c'2 g4 a2 c4
    <b e,>1.
  } \new Voice { \voiceTwo
    c,2. d2.
    e,1._\markup{ \italic { Ped. } }
  } >> \oneVoice
}

choirPart = \new ChoirStaff <<
  \override ChoirStaff.SystemStartBracket.collapse-height = #4
  \new Dynamics \with {
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing =
      #'((basic-distance . 2.5))
  } { \tmControl }
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
    instrumentName = "S."
  } \new Voice = "soprano" { \soprano }
  \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { \sopranoVerse }
  \new Staff = "alto" \with {
    midiInstrument = "choir aahs"
    instrumentName = "A."
  } \new Voice = "alto" { \alto }
  \new Lyrics = "alto-lyrics" \lyricsto "alto" { \altoVerse }
  \new Staff = "tenor" \with {
    midiInstrument = "choir aahs"
    instrumentName = "T."
  } \new Voice = "tenor" { \clef "treble_8" \tenor }
  \new Lyrics = "tenor-lyrics" \lyricsto "tenor" { \tenorVerse }
  \new Staff = "bass" \with {
    midiInstrument = "choir aahs"
    instrumentName = "B."
  } \new Voice = "bass" { \clef bass \bass }
  \new Lyrics = "bass-lyrics" \lyricsto "bass" { \bassVerse }
>>

pianoPart = \new PianoStaff \with {
  instrumentName = "Org."
} <<
  \new Staff = "right" \with {
    midiInstrument = "church organ"
  } \right
  \new Dynamics { \pnControl }
  \new Staff = "left" \with {
    midiInstrument = "church organ"
  } { \clef bass \left }
>>
