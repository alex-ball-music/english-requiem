\relative <<
  \new Staff = "soprano" \with {
    midiInstrument = "choir aahs"
  } <<
    \new Voice = "soprano" {
      \clef treble
      \key g \major
      \relative c''
      {
        \set Score.tempoHideNote = ##t
        \tempo 4=180
        g4( a)
        \tempo 4=90
        b
        \tempo 4=180
        g( a) b( a)
        \tempo 4=90
        a \bar "||"
      }
    }
    \new Lyrics = "soprano-lyrics" \lyricsto "soprano" { The Lord be with you. }
  >>
>>
